#include "command_testing.h"

Command_testing::Command_testing()
{

}

void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command){
    if (*__command != enum_commandReaction_t::ADDITIONAL_COMMAND) {
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_powerCommand_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::POWER_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_CC2650Command_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::CC2650_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_ADS1292RCommand_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::ADS1292R_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_FRAMCommand_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::FRAM_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_BLECommand_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::BLE_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


void Command_testing::create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_powerManagementCommand_t *__addiCommand){
    if (*__peri == enum_peripheralSelection_t::POWERMANAGEMENT_TESTING and *__command == enum_commandReaction_t::ADDITIONAL_COMMAND){
        __set_tcCommand_peripheral(__peri);
        __set_tcCommand_commandReaction(__command);
        __set_tcCommand_additionalCommand(__addiCommand);
    }
}


uint8_t* Command_testing::get_tcCommand(){
    return __tc_mainCommand;
}


uint8_t* Command_testing::get_frCommand(){
    return __fr_mainCommand;
}


void Command_testing::__set_tcCommand_toDefault(){
    *__tc_mainCommand = 0xA5;
    *(__tc_mainCommand+1) = 0x00;
    *(__tc_mainCommand+2) = 0x00;
    *(__tc_mainCommand+3) = 0x5A;
}


void Command_testing::__set_tcCommand_peripheral(enum_peripheralSelection_t *__peri){
    switch (*__peri) {
        case enum_peripheralSelection_t::POWER_TESTING:             *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_POWER_TESTING_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
        case enum_peripheralSelection_t::CC2650_TESTING:            *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_CC2650_TESTING_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
        case enum_peripheralSelection_t::ADS1292R_TESTING:          *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_ADS1292R_COMMUNICATION_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
        case enum_peripheralSelection_t::FRAM_TESTING:              *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_FRAM_COMMUNICATION_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
        case enum_peripheralSelection_t::BLE_TESTING:               *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_BLE_COMMUNICATION_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
        case enum_peripheralSelection_t::POWERMANAGEMENT_TESTING:   *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_POWER_MANAGEMENT_MODE << TC_SHIFT_PERIPHERAL_SELECTION); break;
    }
}


void Command_testing::__set_tcCommand_commandReaction(enum_commandReaction_t *__command){
    switch(*__command){
        case enum_commandReaction_t::DEINIT_COMPONENT:              *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_DEINIT_COMPONENT); break;
        case enum_commandReaction_t::INIT_COMPONENT:                *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_INIT_COMPONENT); break;
        case enum_commandReaction_t::RELEASE_COMMUNICATE_PIN:       *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_RELEASE_COMMUNICATE_PIN); break;
        case enum_commandReaction_t::CONNECT_COMMUNICATE_PIN:       *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_CONNECT_COMMUNICATE_PIN); break;
        case enum_commandReaction_t::SET_TEST_CONFIGURATION:        *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_SET_TEST_CONFIGURATION); break;
        case enum_commandReaction_t::GET_TEST_CONFIGURATION:        *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_GET_TEST_CONFIGURATION); break;
        case enum_commandReaction_t::SEND_BUFFER:                   *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_SEND_BUFFER); break;
        case enum_commandReaction_t::RECEIVE_BUFFER:                *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_RECEIVE_BUFFER); break;
        case enum_commandReaction_t::ADDITIONAL_COMMAND:            *(__tc_mainCommand+1) = *(__tc_mainCommand+1) | (TC_ADDITIONAL_COMMAND); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_powerCommand_t *__addiCommand){
    switch(*__addiCommand){
        case enum_powerCommand_t::POWER_USB_MANAGEMENT:     *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_POWER_USB_MEASUREMENT); break;
        case enum_powerCommand_t::POWER_33_MANAGEMENT:      *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_POWER_33_MEASUREMENT); break;
        case enum_powerCommand_t::POWER_VBATT_MANAGEMENT:   *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_POWER_VBATT_MEASUREMENT); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_CC2650Command_t *__addiCommand){
    switch(*__addiCommand){
        case enum_CC2650Command_t::ENTER_TESTING_MODE:      *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_CC2650_ENTER_TESTING_MODE); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_ADS1292RCommand_t *__addiCommand){
    switch(*__addiCommand){
        case enum_ADS1292RCommand_t::FREQUENCY_TESTING:     *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_FREQUENCY_TESTING); break;
        case enum_ADS1292RCommand_t::SINEWAVE_COMB1:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_SINEWAVE_COMB1); break;
        case enum_ADS1292RCommand_t::SINEWAVE_COMB2:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_SINEWAVE_COMB2); break;
        case enum_ADS1292RCommand_t::SINEWAVE_COMB3:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_SINEWAVE_COMB3); break;
        case enum_ADS1292RCommand_t::SINEWAVE_COMB4:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_SINEWAVE_COMB4); break;
        case enum_ADS1292RCommand_t::ECGFUNC_TEST:          *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_ECGFUNC_TEST); break;
        case enum_ADS1292RCommand_t::IMPEDACNE_TEST:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_IMPEDANCE_TEST); break;
        case enum_ADS1292RCommand_t::LEADOFF_TEST:          *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_ADS_LEADOFF_TEST); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_FRAMCommand_t *__addiCommand){
    switch(*__addiCommand){
        case enum_FRAMCommand_t::WRITE_HEAD_ADDRESS:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_WRITE_HEAD_ADDRESS); break;
        case enum_FRAMCommand_t::WRITE_MIDDLE_ADDRESS:      *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_WRITE_MIDDLE_ADDRESS); break;
        case enum_FRAMCommand_t::WRITE_LAST_ADDRESS:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_WRITE_LAST_ADDRESS); break;
        case enum_FRAMCommand_t::READ_HEAD_ADDRESS:         *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_READ_HEAD_ADDRESS); break;
        case enum_FRAMCommand_t::READ_MIDDLE_ADDRESS:       *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_READ_MIDDLE_ADDRESS); break;
        case enum_FRAMCommand_t::READ_LAST_ADDRESS:         *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_FRAM_READ_LAST_ADDRESS); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_BLECommand_t *__addiCommand){
    switch(*__addiCommand){
        case enum_BLECommand_t::PAIR_BLE:                   *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_BLE_PAIR); break;
        case enum_BLECommand_t::UNPAIR_BLE:                 *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_BLE_UNPAIR); break;
    }
}


void Command_testing::__set_tcCommand_additionalCommand(enum_powerManagementCommand_t *__addiCommand){
    switch(*__addiCommand){
        case enum_powerManagementCommand_t::SET_POWERCOMSUMP_MODE:  *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_POWERMANAGE_POWERCOMSUMP_MODE); break;
        case enum_powerManagementCommand_t::SET_NORMAL_MODE:        *(__tc_mainCommand+2) = *(__tc_mainCommand+2) | (TC_ADDI_POWERMANAGE_NORMAL_MODE); break;
    }
}


void Command_testing::__set_tcCommand_peripheralConfig_ADS1292RConfig(){

}


void Command_testing::__set_tcCommand_peripheralConfig_FRAMConfig(){

}


void Command_testing::__set_tcCommand_sendBuffer_FRAMBuffer(){

}


void Command_testing::__set_tcCommand_sendBuffer_BLEBuffer(){

}


void Command_testing::__check_frCommand_reactStatus(){

}
