#include "prosim2_config.h"



Prosim2_config::Prosim2_config()
{
    __ECGFunction_config = new ECGFunction_config;
    __arrhythmia_config = new arrhythmia_config;
    __perftest_config = new perftest_config;

    __enum_mode = new enum_prosim2Mode_t;
    __enum_ECGFunc_mode = new enum_ECGFunc_mode;
    
    *__enum_mode = ECGFUNC;
    *__enum_ECGFunc_mode = enum_ECGFunc_mode::BPM;

}


enum_prosim2Mode_t Prosim2_config::get_currentMode()
{
    return *__enum_mode;
}

void Prosim2_config::set_currentMode(enum_prosim2Mode_t *__mode)
{
    *__enum_mode = *__mode;
}

struct_ECGFuncConfig_t Prosim2_config::get_currentECGFunction_config()
{
    return __ECGFunction_config->get_currentECGFunction_config();
}


void Prosim2_config::set_currentECGFunction_config(enum_ECGFunc_mode *__mode,int *value)
{
    if(*__mode == BPM)
    {
        if(__ECGFunction_config->get_BPM() != *value) {__ECGFunction_config->set_BPM(value);}
    }   
}


void Prosim2_config::set_currentECGFunction_config(enum_ECGFunc_mode *__mode,double *value)
{
    if(*__mode == AMPLITUDE)
    {
        if(__ECGFunction_config->get_AMPLITUDE() != *value) {__ECGFunction_config->set_AMPLITUDE(value);}
    }
    else if(*__mode == ST)
    {
        if(__ECGFunction_config->get_ST() != *value) {__ECGFunction_config->set_ST(value);}
    }
}


void Prosim2_config::set_currentECGFunction_config(enum_ECGFunc_mode *__mode,bool *value)
{
    if(*__mode == PT_TYPE) 
    {
        if(__ECGFunction_config->get_PT_TYPE() != *value) {__ECGFunction_config->set_PT_TYPE(value);}
    }
}


void Prosim2_config::set_currentECGFunction_config(enum_ECGFunc_mode *__mode,enum_artifact_t *value)
{
    if(*__mode == ARTIFACT_SIM)
    {
        if(__ECGFunction_config->get_ARTIFACT_SIM() != *value) {__ECGFunction_config->set_ARTIFACT_SIM(value);}
    }
}


vector<string> Prosim2_config::get_currentCommand(enum_prosim2Mode_t *__mode)
{
    if(*__mode == enum_prosim2Mode_t::ECGFUNC) { return __ECGFunction_config->get_vector_currentCommand();}
    else if(*__mode == enum_prosim2Mode_t::ARRYTHMIA) { return __arrhythmia_config->get_vector_currentCommand();}
    else if(*__mode == enum_prosim2Mode_t::PERFTEST) { return __perftest_config->get_vector_currentCommand();}
}


void Prosim2_config::set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_supraventricularMode_t *__semimode)
{
    if(*__mode == enum_arrhythmiaMode_t::SUPRAVENTRICULAR)
    {
        __arrhythmia_config->set_struct_currentArrhythmiaMode(__mode, __semimode);
    }
}


void Prosim2_config::set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_prematureMode_t *__semimode)
{
    if(*__mode == enum_arrhythmiaMode_t::PREMATURE)
    {
        __arrhythmia_config->set_struct_currentArrhythmiaMode(__mode, __semimode);
    }
}


void Prosim2_config::set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_ventricularMode_t *__semimode)
{
    if(*__mode == enum_arrhythmiaMode_t::VENTRICULAR)
    {
        __arrhythmia_config->set_struct_currentArrhythmiaMode(__mode, __semimode);
    }
}


void Prosim2_config::set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_conductionDefectMode_t *__semimode)
{
    if(*__mode == enum_arrhythmiaMode_t::CONDUCTION_DEFECT)
    {
        __arrhythmia_config->set_struct_currentArrhythmiaMode(__mode, __semimode);
    }
}


struct_performanceWave_config_t Prosim2_config::get_currentPerformanceWave_config(enum_perfTest_mode_t *__mode)
{
    if(*__mode == enum_perfTest_mode_t::PERFORMANCE_WAVES)
    {
        return  __perftest_config->get_currentPerformanceWave_config(); 
    }
}


struct_R_waveDetection_config_t Prosim2_config::get_current_R_waveDetection_config(enum_perfTest_mode_t *__mode)
{
    if(*__mode == enum_perfTest_mode_t::R_WAVE_DETECTION)
    {
        return __perftest_config->get_current_R_waveDetection_config();
    }
}


void Prosim2_config::set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_performanceWaves_mode_t *__semimode, int *value)
{
    if(*__mode == enum_perfTest_mode_t::PERFORMANCE_WAVES)
    {
        __perftest_config->set_currentPerfTestMode(__mode);
        __perftest_config->set_currentPerformanceWavesMode(__semimode);

        if(*__semimode == enum_performanceWaves_mode_t::PERFORMANCE_WAVEFORM)
        {
            if(*value != __perftest_config->get_PERFORMANCE_WAVES())
            {
            __perftest_config->set_PERFORMANCE_WAVES(value);
            }
        }
    }
}


void Prosim2_config::set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_performanceWaves_mode_t *__semimode, double *value)
{
    if(*__mode == enum_perfTest_mode_t::PERFORMANCE_WAVES)
    {
        __perftest_config->set_currentPerfTestMode(__mode);
        __perftest_config->set_currentPerformanceWavesMode(__semimode);

        if(*__semimode == enum_performanceWaves_mode_t::PERFORMANCE_WAVES_AMPLITUDE)
        {
            if(*value != __perftest_config->get_PERFORMANCE_AMPLITUDE())
            {
            __perftest_config->set_PERFORMANCE_AMPLITUDE(value);
            }
        }
    }
}


void Prosim2_config::set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_R_wave_mode_t *__semimode, int *value)
{
    if(*__mode == enum_perfTest_mode_t::R_WAVE_DETECTION)
    {
        __perftest_config->set_currentPerfTestMode(__mode);
        __perftest_config->set_current_R_wave_mode(__semimode);

        if(*__semimode == enum_R_wave_mode_t::R_WAVE_RATE)
        {
            if(*value != __perftest_config->get_R_WAVE_RATE())
            {
                __perftest_config->set_R_WAVE_RATE(value);
            }
        }

        else if(*__semimode == enum_R_wave_mode_t::R_WAVE_WIDTH)
        {
            if(*value != __perftest_config->get_R_WAVE_WIDTH())
            {
                __perftest_config->set_R_WAVE_WIDTH(value);
            }
        }
    }
}


void Prosim2_config::set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_R_wave_mode_t *__semimode, double *value)
{
    if(*__mode == enum_perfTest_mode_t::R_WAVE_DETECTION)
    {
        __perftest_config->set_currentPerfTestMode(__mode);
        __perftest_config->set_current_R_wave_mode(__semimode);

        if(*__semimode == enum_R_wave_mode_t::R_WAVE_AMPLITUDE)
        {
            if(*value != __perftest_config->get_R_WAVE_AMPLITUDE())
            {
                __perftest_config->set_R_WAVE_AMPLITUDE(value);
            }
        }
    }
}




