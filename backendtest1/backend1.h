
#ifndef BACKEND1_H
#define BACKEND1_H

#define ARDUINOSTART    "connect\n"
#define PROSIMCONNECT   "REMOTE\r"

#include "serialport.h"
#include <QDebug>



class backend : public QObject
{
    Q_OBJECT

public:
    backend();
    void set_serialPort_prosim(struct_settings_t port);
    void set_serialPort_arduino(struct_settings_t port);
    void set_currentPortType(enum_portType currentPort);
    enum_portType get_currentPortType();
    void start_serialConnection();

public slots:
    void connect_serialPort_prosim();
    void connect_serialPort_arduino();


private:
    ~backend();
    serialPort *__prosim_serialPort;
    serialPort *__arduino_serialPort;
    enum_portType *__enum_currentPortType;

};

#endif // BACKEND1_H
