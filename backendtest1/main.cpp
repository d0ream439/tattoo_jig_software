#include "backend1.h"
#include "frontend.h"
#include <QApplication>
#include <QMainWindow>

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);

    Mainwindow *a = new Mainwindow();
    a->show();

    //frontend *mainFrontend = new frontend(mainWindow);
    //mainFrontend->build_allTab(mainWindow);

    app.exec();


}

/*
void addQLabel(QBoxLayout *VLayout,const QString word);
void addQLabel(int row, int column,QTableWidget *table, const QString word);
void addQLabel(int row, int column,QTableWidget *table, const QString word,const QString alignment);
void buildTab1(QWidget *parent);
void buildTab2(QWidget *parent);

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget *window  = new QWidget;
        window->setWindowTitle("Prosim2");
        window->setFixedSize(610, 610);

    QVBoxLayout *vlayout_tabs = new QVBoxLayout;

    QWidget *tab1       = new QWidget();
    QWidget *tab2       = new QWidget();
    QWidget *tab3       = new QWidget();
    QTabWidget *tabs    = new QTabWidget(window);

    tabs->setFixedSize(585, 585);

    tabs->addTab(tab1,"Serial Configuration");
    buildTab1(tab1);

    tabs->addTab(tab2,"Signal");
    buildTab2(tab2);

    tabs->addTab(tab3,"Temperature");


    vlayout_tabs->addWidget(tabs);
    window->setLayout(vlayout_tabs);

    window->show();
    return app.exec();
}

void addQLabel(QBoxLayout *Layout,const QString word){
    QLabel *string    =   new QLabel(word);
    Layout->addWidget(string);
}

void addQLabel(int row, int column,QTableWidget *table, const QString word){
    QLabel *string    =   new QLabel(word);
    table->setCellWidget(row,column,string);
}

void addQLabel(int row, int column,QTableWidget *table, const QString word,const QString alignment){
    QLabel *string    =   new QLabel(word);
    table->setCellWidget(row,column,string);
    if(alignment=="Center")
        string->setAlignment(Qt::AlignCenter);
}

void buildTab1(QWidget *parent){
    QVBoxLayout *vlayout_groupbox = new QVBoxLayout;

    QWidget *widget_upperGroupBox       = new QWidget();
        QHBoxLayout *hlayout_upperGroupBox = new QHBoxLayout;

        QGroupBox *groupbox_selectSerialPortUpper = new QGroupBox("Select Serial Port",parent);
        QVBoxLayout *vlayout_selectSerialPortUpper = new QVBoxLayout;
        QComboBox *comboBox_portUpper             = new QComboBox(groupbox_selectSerialPortUpper);
            comboBox_portUpper->addItem("Serial Port 1");
            comboBox_portUpper->addItem("Serial Port 2");
            comboBox_portUpper->addItem("Serial Port 3");
        vlayout_selectSerialPortUpper->addWidget(comboBox_portUpper);
            addQLabel(vlayout_selectSerialPortUpper,"Description:");
            addQLabel(vlayout_selectSerialPortUpper,"Manufacturer:");
            addQLabel(vlayout_selectSerialPortUpper,"Serial number:");
            addQLabel(vlayout_selectSerialPortUpper,"Location:");
            addQLabel(vlayout_selectSerialPortUpper,"Vendor Identifier:");
            addQLabel(vlayout_selectSerialPortUpper,"Product Identifier:");
        groupbox_selectSerialPortUpper->setLayout(vlayout_selectSerialPortUpper);

        QGroupBox *groupbox_selectParametersUpper = new QGroupBox("Select Parameters",parent);
        QHBoxLayout *hlayout_selectParametersUpper = new QHBoxLayout();
        QWidget *leftWidget_selectParametersUpper = new QWidget();
        QVBoxLayout *vlayout_parametersUpper = new QVBoxLayout();
            addQLabel(vlayout_parametersUpper,"BaudRate:");
            addQLabel(vlayout_parametersUpper,"Data bits:");
            addQLabel(vlayout_parametersUpper,"Parity:");
            addQLabel(vlayout_parametersUpper,"Stop bits:");
            addQLabel(vlayout_parametersUpper,"Flow control:");
        leftWidget_selectParametersUpper->setLayout(vlayout_parametersUpper);
        hlayout_selectParametersUpper->addWidget(leftWidget_selectParametersUpper);

        QWidget *rightWidget_selectParametersUpper = new QWidget();
        QVBoxLayout *vlayout_comboBoxOfParametersUpper = new QVBoxLayout();
        QVBoxLayout *vlayout_selectParametersUpper = new QVBoxLayout();

        QComboBox *comboBox_baudrateUpper = new QComboBox(rightWidget_selectParametersUpper);
            comboBox_baudrateUpper->addItem("9600");
            comboBox_baudrateUpper->addItem("19200");
            comboBox_baudrateUpper->addItem("38400");
            comboBox_baudrateUpper->addItem("115200");
            comboBox_baudrateUpper->addItem("Custom");
        QComboBox *comboBox_dataBitsUpper = new QComboBox(rightWidget_selectParametersUpper);
            comboBox_dataBitsUpper->addItem("5");
            comboBox_dataBitsUpper->addItem("6");
            comboBox_dataBitsUpper->addItem("7");
            comboBox_dataBitsUpper->addItem("8");
        QComboBox *comboBox_parityUpper = new QComboBox(rightWidget_selectParametersUpper);
            comboBox_parityUpper->addItem("None");
            comboBox_parityUpper->addItem("Even");
            comboBox_parityUpper->addItem("Odd");
            comboBox_parityUpper->addItem("Mark");
            comboBox_parityUpper->addItem("Space");
        QComboBox *comboBox_stopBitsUpper = new QComboBox(rightWidget_selectParametersUpper);
            comboBox_stopBitsUpper->addItem("1");
            comboBox_stopBitsUpper->addItem("1.5");
            comboBox_stopBitsUpper->addItem("2");
        QComboBox *comboBox_flowControlUpper = new QComboBox(rightWidget_selectParametersUpper);
            comboBox_flowControlUpper->addItem("None");
            comboBox_flowControlUpper->addItem("RTS/CTS");
            comboBox_flowControlUpper->addItem("XON/XOFF");

        vlayout_comboBoxOfParametersUpper->addWidget(comboBox_baudrateUpper);
        vlayout_comboBoxOfParametersUpper->addWidget(comboBox_dataBitsUpper);
        vlayout_comboBoxOfParametersUpper->addWidget(comboBox_parityUpper);
        vlayout_comboBoxOfParametersUpper->addWidget(comboBox_stopBitsUpper);
        vlayout_comboBoxOfParametersUpper->addWidget(comboBox_flowControlUpper);
        rightWidget_selectParametersUpper->setLayout(vlayout_comboBoxOfParametersUpper);

        hlayout_selectParametersUpper->addWidget(rightWidget_selectParametersUpper);

        QPushButton *pushButton_connectUpper = new QPushButton("Connect");
        vlayout_selectParametersUpper->addLayout(hlayout_selectParametersUpper);
        vlayout_selectParametersUpper->addWidget(pushButton_connectUpper);

        groupbox_selectParametersUpper->setLayout(vlayout_selectParametersUpper);

        hlayout_upperGroupBox->addWidget(groupbox_selectSerialPortUpper);
        hlayout_upperGroupBox->addWidget(groupbox_selectParametersUpper);

        widget_upperGroupBox->setLayout(hlayout_upperGroupBox);

    QWidget *widget_lowerGroupBox       = new QWidget();
        QHBoxLayout *hlayout_lowerGroupBox = new QHBoxLayout;

        QGroupBox *groupbox_selectSerialPortLower = new QGroupBox("Select Serial Port",parent);
        QVBoxLayout *vlayout_selectSerialPortLower = new QVBoxLayout;
        QComboBox *comboBox_portLower             = new QComboBox(groupbox_selectSerialPortLower);
            comboBox_portLower->addItem("Serial Port 1");
            comboBox_portLower->addItem("Serial Port 2");
            comboBox_portLower->addItem("Serial Port 3");
        vlayout_selectSerialPortLower->addWidget(comboBox_portLower);
            addQLabel(vlayout_selectSerialPortLower,"Description:");
            addQLabel(vlayout_selectSerialPortLower,"Manufacturer:");
            addQLabel(vlayout_selectSerialPortLower,"Serial number:");
            addQLabel(vlayout_selectSerialPortLower,"Location:");
            addQLabel(vlayout_selectSerialPortLower,"Vendor Identifier:");
            addQLabel(vlayout_selectSerialPortLower,"Product Identifier:");
        groupbox_selectSerialPortLower->setLayout(vlayout_selectSerialPortLower);

        QGroupBox *groupbox_selectParametersLower = new QGroupBox("Select Parameters",parent);
        QHBoxLayout *hlayout_selectParametersLower = new QHBoxLayout();
        QWidget *leftWidget_selectParametersLower = new QWidget();
        QVBoxLayout *vlayout_parametersLower = new QVBoxLayout();
            addQLabel(vlayout_parametersLower,"BaudRate:");
            addQLabel(vlayout_parametersLower,"Data bits:");
            addQLabel(vlayout_parametersLower,"Parity:");
            addQLabel(vlayout_parametersLower,"Stop bits:");
            addQLabel(vlayout_parametersLower,"Flow control:");
        leftWidget_selectParametersLower->setLayout(vlayout_parametersLower);
        hlayout_selectParametersLower->addWidget(leftWidget_selectParametersLower);

        QWidget *rightWidget_selectParametersLower = new QWidget();
        QVBoxLayout *vlayout_comboBoxOfParametersLower = new QVBoxLayout();
        QVBoxLayout *vlayout_selectParametersLower = new QVBoxLayout();

        QComboBox *comboBox_baudrateLower = new QComboBox(rightWidget_selectParametersLower);
            comboBox_baudrateLower->addItem("9600");
            comboBox_baudrateLower->addItem("19200");
            comboBox_baudrateLower->addItem("38400");
            comboBox_baudrateLower->addItem("115200");
            comboBox_baudrateLower->addItem("Custom");
        QComboBox *comboBox_dataBitsLower = new QComboBox(rightWidget_selectParametersLower);
            comboBox_dataBitsLower->addItem("5");
            comboBox_dataBitsLower->addItem("6");
            comboBox_dataBitsLower->addItem("7");
            comboBox_dataBitsLower->addItem("8");
        QComboBox *comboBox_parityLower = new QComboBox(rightWidget_selectParametersLower);
            comboBox_parityLower->addItem("None");
            comboBox_parityLower->addItem("Even");
            comboBox_parityLower->addItem("Odd");
            comboBox_parityLower->addItem("Mark");
            comboBox_parityLower->addItem("Space");
        QComboBox *comboBox_stopBitsLower = new QComboBox(rightWidget_selectParametersLower);
            comboBox_stopBitsLower->addItem("1");
            comboBox_stopBitsLower->addItem("1.5");
            comboBox_stopBitsLower->addItem("2");
        QComboBox *comboBox_flowControlLower = new QComboBox(rightWidget_selectParametersLower);
            comboBox_flowControlLower->addItem("None");
            comboBox_flowControlLower->addItem("RTS/CTS");
            comboBox_flowControlLower->addItem("XON/XOFF");

        vlayout_comboBoxOfParametersLower->addWidget(comboBox_baudrateLower);
        vlayout_comboBoxOfParametersLower->addWidget(comboBox_dataBitsLower);
        vlayout_comboBoxOfParametersLower->addWidget(comboBox_parityLower);
        vlayout_comboBoxOfParametersLower->addWidget(comboBox_stopBitsLower);
        vlayout_comboBoxOfParametersLower->addWidget(comboBox_flowControlLower);
        rightWidget_selectParametersLower->setLayout(vlayout_comboBoxOfParametersLower);

        hlayout_selectParametersLower->addWidget(rightWidget_selectParametersLower);

        QPushButton *pushButton_connectLower = new QPushButton("Connect");
        vlayout_selectParametersLower->addLayout(hlayout_selectParametersLower);
        vlayout_selectParametersLower->addWidget(pushButton_connectLower);

        groupbox_selectParametersLower->setLayout(vlayout_selectParametersLower);

        hlayout_lowerGroupBox->addWidget(groupbox_selectSerialPortLower);
        hlayout_lowerGroupBox->addWidget(groupbox_selectParametersLower);

        widget_lowerGroupBox->setLayout(hlayout_lowerGroupBox);


    vlayout_groupbox->addWidget(widget_upperGroupBox);
    vlayout_groupbox->addWidget(widget_lowerGroupBox);
    parent->setLayout(vlayout_groupbox);
}


void buildTab2(QWidget *parent){
    QVBoxLayout *vlayout_table1 = new QVBoxLayout;
    QTableWidget *table1    = new QTableWidget(parent);
    table1->setRowCount(4);
    table1->setColumnCount(3);
    table1->setColumnWidth(0, 80);
    table1->setColumnWidth(1, 350);
    table1->setColumnWidth(2, 100);

    table1->setHorizontalHeaderItem(0, new QTableWidgetItem("Selected"));
    table1->setHorizontalHeaderItem(1, new QTableWidgetItem("Order"));
    table1->setHorizontalHeaderItem(2, new QTableWidgetItem("Status"));

    QWidget *checkBoxWidget1 = new QWidget();
    QWidget *checkBoxWidget2 = new QWidget();
    QWidget *checkBoxWidget3 = new QWidget();
    QWidget *checkBoxWidget4 = new QWidget();

    QCheckBox *checkBox1 = new QCheckBox();
    QCheckBox *checkBox2 = new QCheckBox();
    QCheckBox *checkBox3 = new QCheckBox();
    QCheckBox *checkBox4 = new QCheckBox();

    QVBoxLayout *layoutCheckBox1 = new QVBoxLayout(checkBoxWidget1);
    QVBoxLayout *layoutCheckBox2 = new QVBoxLayout(checkBoxWidget2);
    QVBoxLayout *layoutCheckBox3 = new QVBoxLayout(checkBoxWidget3);
    QVBoxLayout *layoutCheckBox4 = new QVBoxLayout(checkBoxWidget4);

    layoutCheckBox1->addWidget(checkBox1);
    layoutCheckBox1->setAlignment(Qt::AlignCenter);
    layoutCheckBox1->setContentsMargins(0,0,0,0);

    layoutCheckBox2->addWidget(checkBox2);
    layoutCheckBox2->setAlignment(Qt::AlignCenter);
    layoutCheckBox2->setContentsMargins(0,0,0,0);

    layoutCheckBox3->addWidget(checkBox3);
    layoutCheckBox3->setAlignment(Qt::AlignCenter);
    layoutCheckBox3->setContentsMargins(0,0,0,0);

    layoutCheckBox4->addWidget(checkBox4);
    layoutCheckBox4->setAlignment(Qt::AlignCenter);
    layoutCheckBox4->setContentsMargins(0,0,0,0);

    table1->setCellWidget(0,0,checkBoxWidget1);
    table1->setCellWidget(1,0,checkBoxWidget2);
    table1->setCellWidget(2,0,checkBoxWidget3);
    table1->setCellWidget(3,0,checkBoxWidget4);

    addQLabel(0,1,table1,"mimimi1");
    addQLabel(1,1,table1,"mimimi2");
    addQLabel(2,1,table1,"mimimi3");
    addQLabel(3,1,table1,"mimimi4");

    QTableWidgetItem *background1 = new QTableWidgetItem();
    addQLabel(0,2,table1,"Fail!!","Center");
    background1->setBackgroundColor("red");
    table1->setItem(0,2,background1);

    QTableWidgetItem *background2 = new QTableWidgetItem();
    addQLabel(1,2,table1,"Pass!!","Center");
    background2->setBackgroundColor("green");
    table1->setItem(1,2,background2);

    QTableWidgetItem *background3 = new QTableWidgetItem();
    addQLabel(2,2,table1,"Untest!!","Center");
    background3->setBackgroundColor("white");
    table1->setItem(2,2,background3);

    vlayout_table1->addWidget(table1);
    parent->setLayout(vlayout_table1);
}

*/
