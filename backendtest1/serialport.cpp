#include "serialport.h"
#include <QDebug>

serialPort::serialPort()
{
    __serialPort_amount = QSerialPortInfo::availablePorts();
    __serialPort = new QSerialPort;
    __serialPortInfo = new QSerialPortInfo;
    __struct_serialPortInfo = new struct_serialPortInfo_t;
    __vector_AllSerialPort = new QVector <QString>;
    __serialPortSettings = new struct_settings_t;
}


void serialPort::set_struct_currentSerialPortInfo(struct_serialPortInfo_t *info)
{
    info->serial_portName           = __serialPortInfo->portName();
    info->serial_description        = __serialPortInfo->description();
    info->serial_systemLocation     = __serialPortInfo->systemLocation();
    info->serial_serialNumber       = __serialPortInfo->serialNumber();
    info->serial_manufacturer       = __serialPortInfo->manufacturer();
    info->serial_vendorIdentifier   = __serialPortInfo->hasVendorIdentifier() ? QString::number(__serialPortInfo->vendorIdentifier(), 16) : QString();
    info->serial_productIdentifier  = __serialPortInfo->hasProductIdentifier()? QString::number(__serialPortInfo->productIdentifier(),16) : QString();
}


struct_serialPortInfo_t serialPort::get_struct_currentSerialPortInfo()
{
    return *__struct_serialPortInfo;
}




void serialPort::set_struct_value_currentSerialPortSettings(struct_settings_t settings)
{
    settings.name          = __serialPort->portName();
    settings.baudRate      = __serialPort->baudRate();
    settings.parity        = __serialPort->parity();
    settings.dataBits      = __serialPort->dataBits();
    settings.stopBits      = __serialPort->stopBits();
    settings.flowControl   = __serialPort->flowControl();
}


void serialPort::set_struct_string_currentSerialPortSettings(struct_settings_t settings)
{
    settings.stringBaudRate    = QString::number(__serialPort->baudRate());
    settings.stringDataBits    = QString::number(__serialPort->dataBits());
    settings.stringStopBits    = QString::number(__serialPort->stopBits());
    settings.stringParity      = QString::number(__serialPort->parity());
    settings.stringFlowControl = QString(__serialPort->flowControl());
}


struct_settings_t serialPort::get_struct_currentSerialPortSettings()
{
    return *__serialPortSettings;
}


void serialPort::set_current_serialPortpointer(QString portname)
{
    __serialPortInfo = new QSerialPortInfo(portname);
    __serialPort = new QSerialPort(*__serialPortInfo);
}


void serialPort::set_serialPort_setting(struct_settings_t port)
{
    __serialPort->setPortName(port.name);
    __serialPort->setBaudRate(port.baudRate);
    __serialPort->setDataBits(port.dataBits);
    __serialPort->setStopBits(port.stopBits);
    __serialPort->setParity(port.parity);

    qDebug() <<     __serialPort->portName();
    //qDebug() <<     __serialPort->parity();
    //qDebug() <<     __serialPort->baudRate();
}


void serialPort::set_portType(enum_portType portType)
{
    __enum_portType = &portType;
    qDebug() << "portType = " << get_currentPortType() ;
}


enum_portType serialPort::get_currentPortType()
{
    return *__enum_portType;
}

void serialPort::open_serialPort()
{
   qDebug() <<"------------";
   __serialPort->open(QIODevice::ReadWrite);
   __serialPort->setPortName(__serialPortSettings->name);
   qDebug() << __serialPort->portName();

   /*//success
   __serialPort->write("\n");
   __serialPort->waitForReadyRead(100);
   qDebug()<<"Response: " <<__serialPort->readLine();
    //qDebug() << b;
    qDebug()<<"loop";
    //*/

   //write_serialPort("connect\n");
   //read_serialPort(); //test

   qDebug() << __serialPort->isOpen();
   qDebug() << __serialPort->isReadable();
   qDebug() << __serialPort->isWritable();
}


void serialPort::write_serialPort(const QByteArray data)
{
    __serialPort->write(data);
    __serialPort->waitForBytesWritten(200);
    __serialPort->flush();
}


const QByteArray serialPort::read_serialPort()
{
    __serialPort->waitForReadyRead(200);
    qDebug()<<"Response: " <<__serialPort->readLine();
    return __serialPort->readLine();
}


void serialPort::disconnect_serialPort()
{

}




