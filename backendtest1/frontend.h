#ifndef FRONTEND_H
#define FRONTEND_H


#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QtCore>
#include <QTabWidget>
#include <QDialog>
#include <QMainWindow>
#include <QTableWidget>
#include <QCheckBox>
#include <QGroupBox>
#include <QComboBox>
#include <QLayoutItem>
#include <QObject>

#include "serialport.h"
#include "backend1.h"


class Mainwindow;


class UI_serialSetting_vBoxLayout: public QObject
{
    Q_OBJECT

public:
    explicit UI_serialSetting_vBoxLayout(QWidget *parent);
    QWidget     *widget_serialConfig;
    QVBoxLayout *vBoxLayout_serialConfig;
    QWidget     *vBoxWidget_serialInfo;
    QWidget     *vBoxWideget_serialSetting;
    QHBoxLayout *hBoxLayout_serialConfig;
    QGroupBox   *groupBox_serialInfo;
    QGroupBox   *groupBox_serialSetting;
    QVBoxLayout *vBoxLayout_serialInfo;
    QVBoxLayout *vBoxLayout_serialSetting;
    QComboBox   *comboBox_serialInfo;
    QComboBox   *comboBox_baudrate;
    QComboBox   *comboBox_dataBits;
    QComboBox   *comboBox_parity;
    QComboBox   *comboBox_stopBits;
    QComboBox   *comboBox_flowControl;
    QLabel      *label_description;
    QLabel      *label_manufacterer;
    QLabel      *label_serialNumber;
    QLabel      *label_systemLocation;
    QLabel      *label_vendorIdentifier;
    QLabel      *label_productIdentifier;
    ~UI_serialSetting_vBoxLayout();
};




class serialSetting_vBoxLayout : public QObject
{
    Q_OBJECT

public:
    explicit serialSetting_vBoxLayout(QWidget *parent);
    void create_serialSetting_vBoxlayout(QWidget *parent);
    struct_settings_t return_serialPortSetting_upper();
    struct_settings_t return_serialPortSetting_lower();
    void connect_buttonTobackEnd(backend *backend);
    void connect_buttonToMainwindow(Mainwindow *parent);

public slots:
    void set_portInfo_upper(int index);
    void fill_portInfo_upper();
    void set_portInfo_lower(int index);
    void fill_portInfo_lower();

    void fill_portSetting_upper();
    void fill_portSetting_lower();

    void update_portSetting_upper();
    void update_portSetting_lower();

private:
    ~serialSetting_vBoxLayout();
    UI_serialSetting_vBoxLayout *__ui_serialsetting_upper;
    UI_serialSetting_vBoxLayout *__ui_serialsetting_lower;
    serialPort *__serialPort;
    QVector <QString> *__vector_string_allSerialport;
    struct_serialPortInfo_t *__struct_portInfo;
    struct_settings_t *__struct_serialPortSetting_upper;
    struct_settings_t *__struct_serialPortSetting_lower;
    QPushButton *__button_connect_upper;
    QPushButton *__button_connect_lower;
};


class UI_ADS_operation_tableLayout : public QObject{
    Q_OBJECT

public:
    explicit UI_ADS_operation_tableLayout(QWidget *parent);
    QTableWidget *tableWidget_ADS_operation;

private:
    ~UI_ADS_operation_tableLayout();    
};


class ADS_operation_tableLayout : public QObject{
    Q_OBJECT

public:
    explicit ADS_operation_tableLayout(QWidget *parent);
    void create_ADS_operation_tableLayout(QWidget *parent);

private:
    ~ADS_operation_tableLayout();
    UI_ADS_operation_tableLayout *__ui_ADS_operation_tableLayout;
};


class Mainwindow :public QMainWindow
{
    Q_OBJECT

public:
    explicit Mainwindow();
    void build_allTab(QWidget *parent);
    void build_serialConfigTab(QWidget *parent);
    void build_ADS_operationTab(QWidget *parent);
    void build_FRAM_operationTab(QWidget *parent);
    void build_bluetoothConfigTab(QWidget *parent);
    void build_powerModeTab(QWidget *parent);
    int return_combobox_index();
    struct_settings_t get_serialPort_upper();
    struct_settings_t get_serialPort_lower();

public slots:
    void connect_serialPort_prosim();
    void connect_serialPort_arduino();

private:
    ~Mainwindow();
    QWidget *__tab_serialConfig;
    QWidget *__tab_ADS_operation;
    QWidget *__tab_FRAM_operation;
    QWidget *__tab_bluetoothConfig;
    QWidget *__tab_powerMode;
    QTabWidget *__tabWidget;
    QVBoxLayout *__mainLayout;
    serialPort *__serialPort;
    int __combobox_index;
    QVBoxLayout *vlayout_selectSerialPortUpper;
    QVector<QString> allSerialPort;
    QGroupBox *groupbox_selectSerialPortUpper;
    serialSetting_vBoxLayout *__layout_serialConfig;
    ADS_operation_tableLayout *__layout_ADS_operation;
    backend *__backend;
};


#endif // FRONTEND_H
