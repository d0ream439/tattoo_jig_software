#include "backend1.h"
#include "prosim2_config.h"
#include "command_testing.h"

backend::backend()
{
    __prosim_serialPort     = new serialPort;
    __arduino_serialPort    = new serialPort;
    qDebug() << TC_DEINIT_COMPONENT;
    qDebug() << (TC_DEINIT_COMPONENT << TC_SHIFT_COMMAND_REACTION);
}


void backend::set_serialPort_prosim(struct_settings_t port)
{
    __prosim_serialPort->set_current_serialPortpointer(port.name);
    __prosim_serialPort->set_serialPort_setting(port);
}


void backend::set_serialPort_arduino(struct_settings_t port)
{
    __arduino_serialPort->set_current_serialPortpointer(port.name);
    __arduino_serialPort->set_serialPort_setting(port);
}

void backend::connect_serialPort_prosim()
{
    __prosim_serialPort->open_serialPort();
    __prosim_serialPort->set_portType(enum_portType::PROSIM2);
    qDebug() << "**connect Prosim***" ;
    set_currentPortType(enum_portType::PROSIM2);
    start_serialConnection();

    /*
    __prosim_serialPort->write_serialPort("REMOTE\r");
    __prosim_serialPort->read_serialPort();
    */
}


void backend::connect_serialPort_arduino()
{
    __arduino_serialPort->open_serialPort();
    __arduino_serialPort->set_portType(enum_portType::ARDUINO_DUE);
    qDebug() << "**connect arduino ***" ;
    set_currentPortType(enum_portType::ARDUINO_DUE);
    start_serialConnection();

    /*
    __arduino_serialPort->write_serialPort("\n");
    __arduino_serialPort->read_serialPort();
    */

}


void backend::set_currentPortType(enum_portType currentPort)
{
    __enum_currentPortType = &currentPort;
}


enum_portType backend::get_currentPortType()
{
    return *__enum_currentPortType;
}


void backend::start_serialConnection()
{
    if(get_currentPortType() == enum_portType::ARDUINO_DUE)
    {
        __arduino_serialPort->write_serialPort("\n");
        __arduino_serialPort->read_serialPort();
    }

    else if(get_currentPortType() == enum_portType::PROSIM2)
    {
        __prosim_serialPort->write_serialPort(PROSIMCONNECT);
        __prosim_serialPort->read_serialPort();
    }

    else qDebug() << "ERROR";
}


backend::~backend()
{


}



