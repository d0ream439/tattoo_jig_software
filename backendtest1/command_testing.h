#ifndef COMMAND_TESTING_H
#define COMMAND_TESTING_H

#include <iostream>
#include <vector>
#include <string>

#define TC_MAIN_COMMAND                         {0xA5, 0x00, 0x00, 0x5A}

#define TC_SHIFT_PERIPHERAL_SELECTION           4              // Shift for 0x00000
#define TC_SHIFT_COMMAND_REACTION               0              // Shift for 0x0000
#define TC_SHIFT_ADDITONAL_COMMAND              0               // Shift for 0x00

#define TC_POWER_TESTING_MODE                   0x1
#define TC_CC2650_TESTING_MODE                  0x2
#define TC_ADS1292R_COMMUNICATION_MODE          0x3
#define TC_FRAM_COMMUNICATION_MODE              0x4
#define TC_BLE_COMMUNICATION_MODE               0x5
#define TC_POWER_MANAGEMENT_MODE                0x6

#define TC_DEINIT_COMPONENT                     0x2
#define TC_INIT_COMPONENT                       0x3
#define TC_RELEASE_COMMUNICATE_PIN              0x4
#define TC_CONNECT_COMMUNICATE_PIN              0x5
#define TC_SET_TEST_CONFIGURATION               0x6
#define TC_GET_TEST_CONFIGURATION               0x7
#define TC_SEND_BUFFER                          0x8
#define TC_RECEIVE_BUFFER                       0x9
#define TC_ADDITIONAL_COMMAND                   0xb

#define TC_ADDI_NOCOMMMAND                      0x0

#define TC_ADDI_POWER_USB_MEASUREMENT           0x1
#define TC_ADDI_POWER_33_MEASUREMENT            0x2
#define TC_ADDI_POWER_VBATT_MEASUREMENT         0x3

#define TC_ADDI_CC2650_ENTER_TESTING_MODE       0x1

#define TC_ADDI_ADS_FREQUENCY_TESTING           0x1
#define TC_ADDI_ADS_SINEWAVE_COMB1              0x2
#define TC_ADDI_ADS_SINEWAVE_COMB2              0x3
#define TC_ADDI_ADS_SINEWAVE_COMB3              0x4
#define TC_ADDI_ADS_SINEWAVE_COMB4              0x5
#define TC_ADDI_ADS_ECGFUNC_TEST                0x6
#define TC_ADDI_ADS_IMPEDANCE_TEST              0x7
#define TC_ADDI_ADS_LEADOFF_TEST                0x8

#define TC_ADDI_FRAM_WRITE_HEAD_ADDRESS         0x1
#define TC_ADDI_FRAM_WRITE_MIDDLE_ADDRESS       0x2
#define TC_ADDI_FRAM_WRITE_LAST_ADDRESS         0x3
#define TC_ADDI_FRAM_READ_HEAD_ADDRESS          0x4
#define TC_ADDI_FRAM_READ_MIDDLE_ADDRESS        0x5
#define TC_ADDI_FRAM_READ_LAST_ADDRESS          0x6

#define TC_ADDI_BLE_UNPAIR                      0x2
#define TC_ADDI_BLE_PAIR                        0x3

#define TC_ADDI_POWERMANAGE_NORMAL_MODE         0x2
#define TC_ADDI_POWERMANAGE_POWERCOMSUMP_MODE   0x3


#define FR_MAIN_COMMAND                         {0x5A, 0x00, 0x00, 0x00, 0xA5}

#define FR_RETURN_STATUS_FALSE                  0x0
#define FR_RETURN_STATUS_TRUE                   0x1

#define FR_ERROR_HANDLING_NO_HANDLING           0x0
#define FR_ERROR_HANDLING_1                     0x1

typedef enum: int {
    POWER_TESTING = TC_POWER_TESTING_MODE,
    CC2650_TESTING = TC_CC2650_TESTING_MODE,
    ADS1292R_TESTING = TC_ADS1292R_COMMUNICATION_MODE,
    FRAM_TESTING = TC_FRAM_COMMUNICATION_MODE,
    BLE_TESTING = TC_BLE_COMMUNICATION_MODE,
    POWERMANAGEMENT_TESTING = TC_POWER_MANAGEMENT_MODE

}enum_peripheralSelection_t;


typedef enum: int {
    DEINIT_COMPONENT = TC_DEINIT_COMPONENT,
    INIT_COMPONENT = TC_INIT_COMPONENT,
    RELEASE_COMMUNICATE_PIN = TC_RELEASE_COMMUNICATE_PIN,
    CONNECT_COMMUNICATE_PIN = TC_CONNECT_COMMUNICATE_PIN,
    SET_TEST_CONFIGURATION = TC_SET_TEST_CONFIGURATION,
    GET_TEST_CONFIGURATION = TC_GET_TEST_CONFIGURATION,
    SEND_BUFFER = TC_SEND_BUFFER,
    RECEIVE_BUFFER = TC_RECEIVE_BUFFER,
    ADDITIONAL_COMMAND = TC_ADDITIONAL_COMMAND

}enum_commandReaction_t;


typedef enum: int {
    POWER_USB_MANAGEMENT = TC_ADDI_POWER_USB_MEASUREMENT,
    POWER_33_MANAGEMENT = TC_ADDI_POWER_33_MEASUREMENT,
    POWER_VBATT_MANAGEMENT = TC_ADDI_POWER_VBATT_MEASUREMENT,

}enum_powerCommand_t;


typedef enum: int {
    ENTER_TESTING_MODE = TC_ADDI_CC2650_ENTER_TESTING_MODE

}enum_CC2650Command_t;


typedef enum: int {
    FREQUENCY_TESTING = TC_ADDI_ADS_FREQUENCY_TESTING,
    SINEWAVE_COMB1 = TC_ADDI_ADS_SINEWAVE_COMB1,
    SINEWAVE_COMB2 = TC_ADDI_ADS_SINEWAVE_COMB2,
    SINEWAVE_COMB3 = TC_ADDI_ADS_SINEWAVE_COMB3,
    SINEWAVE_COMB4 = TC_ADDI_ADS_SINEWAVE_COMB4,
    ECGFUNC_TEST = TC_ADDI_ADS_ECGFUNC_TEST,
    IMPEDACNE_TEST = TC_ADDI_ADS_IMPEDANCE_TEST,
    LEADOFF_TEST = TC_ADDI_ADS_LEADOFF_TEST

}enum_ADS1292RCommand_t;


typedef enum:int {
    WRITE_HEAD_ADDRESS = TC_ADDI_FRAM_WRITE_HEAD_ADDRESS,
    WRITE_MIDDLE_ADDRESS = TC_ADDI_FRAM_WRITE_MIDDLE_ADDRESS,
    WRITE_LAST_ADDRESS = TC_ADDI_FRAM_WRITE_LAST_ADDRESS,
    READ_HEAD_ADDRESS = TC_ADDI_FRAM_READ_HEAD_ADDRESS,
    READ_MIDDLE_ADDRESS = TC_ADDI_FRAM_READ_MIDDLE_ADDRESS,
    READ_LAST_ADDRESS = TC_ADDI_FRAM_READ_LAST_ADDRESS

}enum_FRAMCommand_t;


typedef enum:int {
    UNPAIR_BLE = TC_ADDI_BLE_UNPAIR,
    PAIR_BLE = TC_ADDI_BLE_PAIR

}enum_BLECommand_t;


typedef enum:int {
    SET_NORMAL_MODE = TC_ADDI_POWERMANAGE_NORMAL_MODE,
    SET_POWERCOMSUMP_MODE = TC_ADDI_POWERMANAGE_POWERCOMSUMP_MODE

}enum_powerManagementCommand_t;


class Command_testing
{
    public:
        Command_testing();

        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_powerCommand_t *__addiCommand);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_CC2650Command_t *__addiCommand);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_ADS1292RCommand_t *__addiCommand);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_FRAMCommand_t *__addiCommand);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_BLECommand_t *__addiCommand);
        void create_tcCommand(enum_peripheralSelection_t *__peri, enum_commandReaction_t *__command, enum_powerManagementCommand_t *__addiCommand);
        uint8_t* get_tcCommand();
        uint8_t* get_frCommand();

    private:
        void __set_tcCommand_toDefault();
        void __set_tcCommand_peripheral(enum_peripheralSelection_t *__peri);
        void __set_tcCommand_commandReaction(enum_commandReaction_t *__command);
        void __set_tcCommand_additionalCommand(enum_powerCommand_t *__addiCommand);
        void __set_tcCommand_additionalCommand(enum_CC2650Command_t *__addiCommand);
        void __set_tcCommand_additionalCommand(enum_ADS1292RCommand_t *__addiCommand);
        void __set_tcCommand_additionalCommand(enum_FRAMCommand_t *__addiCommand);
        void __set_tcCommand_additionalCommand(enum_BLECommand_t *__addiCommand);
        void __set_tcCommand_additionalCommand(enum_powerManagementCommand_t *__addiCommand);

        void __set_tcCommand_peripheralConfig_ADS1292RConfig();
        void __set_tcCommand_peripheralConfig_FRAMConfig();
        void __set_tcCommand_sendBuffer_FRAMBuffer();
        void __set_tcCommand_sendBuffer_BLEBuffer();

        void __check_frCommand_reactStatus();

        enum_peripheralSelection_t *__enum_peripheralSelection = nullptr;

        uint8_t __tc_mainCommand[4] = TC_MAIN_COMMAND;
        uint8_t __fr_mainCommand[5] = FR_MAIN_COMMAND;

        const uint8_t __ADS1292R_config[11] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        const uint8_t __FRAM_config[1] = {0x00};
};

#endif // COMMAND_TESTING_H
