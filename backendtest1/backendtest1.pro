SOURCES += \
    command_testing.cpp \
    main.cpp \
    backend1.cpp \
    serialport.cpp\
    prosim2_config.cpp\
    ecgfunction.cpp\
    perftest.cpp\
    arrhythmia.cpp\
    frontend.cpp

QT += widgets

QT += serialport

QT += core

HEADERS += \
    backend1.h \
    command_testing.h \
    serialport.h\
    prosim2_config.h\
    ecgfunction.h\
    perftest.h\
    arrhythmia.h\
    frontend.h

