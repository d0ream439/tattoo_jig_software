#include "frontend.h"
#include "backend1.h"

Mainwindow::Mainwindow()
{
    __tab_serialConfig      = new QWidget();
    __tab_FRAM_operation    = new QWidget();
    __tab_ADS_operation     = new QWidget();
    __tab_bluetoothConfig   = new QWidget();
    __tab_powerMode         = new QWidget();

    __tabWidget             = new QTabWidget(this);
    __mainLayout            = new QVBoxLayout(this);
    __serialPort            = new serialPort;
    __backend               = new backend;
    build_allTab(this);
    setFixedSize(720,700);
    __layout_serialConfig->connect_buttonTobackEnd(__backend);
    __layout_serialConfig->connect_buttonToMainwindow(this);


}


void Mainwindow::build_allTab(QWidget *parent)
{
    __tabWidget->addTab(__tab_serialConfig,"Serial Configuration");
    __tabWidget->addTab(__tab_ADS_operation,"ADS1292R Operation");
    __tabWidget->addTab(__tab_FRAM_operation, "FRAM Opearion");
    __tabWidget->addTab(__tab_bluetoothConfig,"Bluetooth Configuration");
    __tabWidget->addTab(__tab_powerMode, "Power Mode Testing");

    build_serialConfigTab(__tab_serialConfig);
    build_ADS_operationTab(__tab_ADS_operation);

    __tabWidget->setFixedSize(720,700);

    __mainLayout->addWidget(__tabWidget);

    parent->setLayout(__mainLayout);
}


void Mainwindow::build_serialConfigTab(QWidget *parent)
{
    __layout_serialConfig   = new serialSetting_vBoxLayout(parent);
}


void Mainwindow::build_ADS_operationTab(QWidget *parent)
{
    __layout_ADS_operation = new ADS_operation_tableLayout(parent);
}


void Mainwindow::build_FRAM_operationTab(QWidget *parent)
{

}


void Mainwindow::build_bluetoothConfigTab(QWidget *parent)
{

}


void Mainwindow::build_powerModeTab(QWidget *parent)
{

}


struct_settings_t Mainwindow::get_serialPort_upper()
{
    return __layout_serialConfig->return_serialPortSetting_upper();
}


struct_settings_t Mainwindow::get_serialPort_lower()
{
    return __layout_serialConfig->return_serialPortSetting_lower();
}


void Mainwindow::connect_serialPort_prosim()
{
    __layout_serialConfig->update_portSetting_upper();
    __backend->set_serialPort_prosim(__layout_serialConfig->return_serialPortSetting_upper());
    struct_settings_t test1 = __layout_serialConfig->return_serialPortSetting_upper();    
    __backend->connect_serialPort_prosim();

    qDebug() << "upper";
    qDebug() << test1.baudRate;
    qDebug() << test1.dataBits;
    qDebug() << test1.parity;

}


void Mainwindow::connect_serialPort_arduino()
{
    __layout_serialConfig->update_portSetting_lower();
    __backend->set_serialPort_arduino(__layout_serialConfig->return_serialPortSetting_lower());
    struct_settings_t test2 = __layout_serialConfig->return_serialPortSetting_lower();
    __backend->connect_serialPort_arduino();

    qDebug() << "lower";
    qDebug() << test2.baudRate;
    qDebug() << test2.dataBits;
    qDebug() << test2.parity;
}


Mainwindow::~Mainwindow()
{

}


UI_serialSetting_vBoxLayout::UI_serialSetting_vBoxLayout(QWidget *parent)
{
    widget_serialConfig         = new QWidget(parent);
    vBoxLayout_serialConfig     = new QVBoxLayout;
    vBoxWidget_serialInfo       = new QWidget();
    vBoxWideget_serialSetting   = new QWidget();
    hBoxLayout_serialConfig     = new QHBoxLayout;
    groupBox_serialInfo         = new QGroupBox("Select Serial Port",parent);
    groupBox_serialSetting      = new QGroupBox("Select Parameters",parent);
    vBoxLayout_serialInfo       = new QVBoxLayout(groupBox_serialInfo);
    vBoxLayout_serialSetting    = new QVBoxLayout(groupBox_serialSetting);
    comboBox_serialInfo         = new QComboBox(groupBox_serialInfo);
    comboBox_baudrate           = new QComboBox(groupBox_serialSetting);
    comboBox_dataBits           = new QComboBox(groupBox_serialSetting);
    comboBox_parity             = new QComboBox(groupBox_serialSetting);
    comboBox_stopBits           = new QComboBox(groupBox_serialSetting);
    label_description           = new QLabel(groupBox_serialInfo);
    label_manufacterer          = new QLabel(groupBox_serialInfo);
    label_serialNumber          = new QLabel(groupBox_serialInfo);
    label_systemLocation        = new QLabel(groupBox_serialInfo);
    label_vendorIdentifier      = new QLabel(groupBox_serialInfo);
    label_productIdentifier     = new QLabel(groupBox_serialInfo);

    groupBox_serialInfo->setFixedSize(400,250);
    groupBox_serialSetting->setFixedSize(250,250);
}


UI_serialSetting_vBoxLayout::~UI_serialSetting_vBoxLayout()
{

}


serialSetting_vBoxLayout::serialSetting_vBoxLayout(QWidget *parent)
{
    __ui_serialsetting_upper                = new UI_serialSetting_vBoxLayout(parent);
    __ui_serialsetting_lower                = new UI_serialSetting_vBoxLayout(parent);
    __serialPort                            = new serialPort;
    __vector_string_allSerialport           = new QVector<QString>;
    __struct_portInfo                       = new struct_serialPortInfo_t;
    __struct_serialPortSetting_upper        = new struct_settings_t;
    __struct_serialPortSetting_lower        = new struct_settings_t;
    __button_connect_upper                  = new QPushButton("Connect Prosim");
    __button_connect_lower                  = new QPushButton("Connect Arduino");

    create_serialSetting_vBoxlayout(parent);
}


void serialSetting_vBoxLayout::create_serialSetting_vBoxlayout(QWidget *parent)
{
    QVBoxLayout *a = new QVBoxLayout(parent);

    connect(__ui_serialsetting_upper->comboBox_serialInfo , QOverload<int>::of(&QComboBox::currentIndexChanged), this ,&serialSetting_vBoxLayout::set_portInfo_upper);
    connect(__ui_serialsetting_lower->comboBox_serialInfo ,QOverload<int>::of(&QComboBox::currentIndexChanged), this ,&serialSetting_vBoxLayout::set_portInfo_lower);

    fill_portInfo_upper();
    fill_portSetting_upper();
    update_portSetting_upper();


    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->comboBox_serialInfo);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_description);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_manufacterer);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_serialNumber);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_systemLocation);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_vendorIdentifier);
    __ui_serialsetting_upper->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_upper->label_productIdentifier);

    __ui_serialsetting_upper->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_upper->comboBox_baudrate);
    __ui_serialsetting_upper->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_upper->comboBox_parity);
    __ui_serialsetting_upper->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_upper->comboBox_dataBits);
    __ui_serialsetting_upper->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_upper->comboBox_stopBits);

    __ui_serialsetting_upper->groupBox_serialInfo->setLayout(__ui_serialsetting_upper->vBoxLayout_serialInfo);
    __ui_serialsetting_upper->groupBox_serialSetting->setLayout(__ui_serialsetting_upper->vBoxLayout_serialSetting);

    __ui_serialsetting_upper->hBoxLayout_serialConfig->addWidget(__ui_serialsetting_upper->groupBox_serialInfo);
    __ui_serialsetting_upper->hBoxLayout_serialConfig->addWidget(__ui_serialsetting_upper->groupBox_serialSetting);

    __ui_serialsetting_upper->vBoxWideget_serialSetting->setLayout(__ui_serialsetting_upper->hBoxLayout_serialConfig);

    __ui_serialsetting_upper->vBoxLayout_serialConfig->addWidget(__ui_serialsetting_upper->vBoxWideget_serialSetting);


    __ui_serialsetting_upper->widget_serialConfig->setLayout(__ui_serialsetting_upper->vBoxLayout_serialConfig);


    fill_portInfo_lower();
    fill_portSetting_lower();
    update_portSetting_lower();


    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->comboBox_serialInfo);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_description);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_manufacterer);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_serialNumber);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_systemLocation);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_vendorIdentifier);
    __ui_serialsetting_lower->vBoxLayout_serialInfo->addWidget(__ui_serialsetting_lower->label_productIdentifier);

    __ui_serialsetting_lower->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_lower->comboBox_baudrate);
    __ui_serialsetting_lower->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_lower->comboBox_parity);
    __ui_serialsetting_lower->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_lower->comboBox_dataBits);
    __ui_serialsetting_lower->vBoxLayout_serialSetting->addWidget(__ui_serialsetting_lower->comboBox_stopBits);

    __ui_serialsetting_lower->groupBox_serialInfo->setLayout(__ui_serialsetting_lower->vBoxLayout_serialInfo);
    __ui_serialsetting_lower->groupBox_serialSetting->setLayout(__ui_serialsetting_lower->vBoxLayout_serialSetting);

    __ui_serialsetting_lower->hBoxLayout_serialConfig->addWidget(__ui_serialsetting_lower->groupBox_serialInfo);
    __ui_serialsetting_lower->hBoxLayout_serialConfig->addWidget(__ui_serialsetting_lower->groupBox_serialSetting);

    __ui_serialsetting_lower->vBoxWideget_serialSetting->setLayout(__ui_serialsetting_lower->hBoxLayout_serialConfig);

    __ui_serialsetting_lower->vBoxLayout_serialConfig->addWidget(__ui_serialsetting_lower->vBoxWideget_serialSetting);

    __ui_serialsetting_lower->widget_serialConfig->setLayout(__ui_serialsetting_lower->vBoxLayout_serialConfig);


    a->addWidget(__ui_serialsetting_upper->widget_serialConfig);
    a->addWidget(__button_connect_upper);
    a->addWidget(__ui_serialsetting_lower->widget_serialConfig);
    a->addWidget(__button_connect_lower);

}

struct_settings_t serialSetting_vBoxLayout::return_serialPortSetting_upper()
{
    return *__struct_serialPortSetting_upper;
}


struct_settings_t serialSetting_vBoxLayout::return_serialPortSetting_lower()
{
    return *__struct_serialPortSetting_lower;
}


void serialSetting_vBoxLayout::connect_buttonTobackEnd(backend *backend)
{
    /*
    connect(__button_connect_upper,&QPushButton::clicked,backend,&backend::connect_serialPort_prosim);
    connect(__button_connect_lower,&QPushButton::clicked,backend,&backend::connect_serialPort_arduino);
    */
}


void serialSetting_vBoxLayout::connect_buttonToMainwindow(Mainwindow *parent)
{
    connect(__button_connect_upper,&QPushButton::clicked,parent,&Mainwindow::connect_serialPort_prosim);
    connect(__button_connect_lower,&QPushButton::clicked,parent,&Mainwindow::connect_serialPort_arduino);
}

void serialSetting_vBoxLayout::set_portInfo_upper(int index)
{
    if(index == -1)
        return;
    const QStringList list = __ui_serialsetting_upper->comboBox_serialInfo->itemData(index).toStringList();
    __ui_serialsetting_upper->label_description->setText("Description : " + list.at(1));
    __ui_serialsetting_upper->label_manufacterer->setText("Manufacterer : " + list.at(2));
    __ui_serialsetting_upper->label_serialNumber->setText("Serial Number :" + list.at(3));
    __ui_serialsetting_upper->label_systemLocation->setText("Location :" + list.at(4));
    __ui_serialsetting_upper->label_vendorIdentifier->setText("Vendor identifier :" + list.at(5));
    __ui_serialsetting_upper->label_productIdentifier->setText("Product identifier :" + list.at(6));

    update_portSetting_upper();
}


void serialSetting_vBoxLayout::fill_portInfo_upper()
{
    __ui_serialsetting_upper->comboBox_serialInfo->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info :infos)
    {
        QStringList list;
        __serialPort->set_current_serialPortpointer(info.portName());
        __serialPort->set_struct_currentSerialPortInfo(__struct_portInfo);

        list << __struct_portInfo->serial_portName
             << __struct_portInfo->serial_description
             << __struct_portInfo->serial_manufacturer
             << __struct_portInfo->serial_serialNumber
             << __struct_portInfo->serial_systemLocation
             << __struct_portInfo->serial_vendorIdentifier
             << __struct_portInfo->serial_productIdentifier;

        __ui_serialsetting_upper->comboBox_serialInfo->addItem(list.first(), list);
    }
}


void serialSetting_vBoxLayout::fill_portInfo_lower()
{
    __ui_serialsetting_lower->comboBox_serialInfo->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info :infos)
    {
        QStringList list;
        __serialPort->set_current_serialPortpointer(info.portName());
        __serialPort->set_struct_currentSerialPortInfo(__struct_portInfo);

        list << __struct_portInfo->serial_portName
             << __struct_portInfo->serial_description
             << __struct_portInfo->serial_manufacturer
             << __struct_portInfo->serial_serialNumber
             << __struct_portInfo->serial_systemLocation
             << __struct_portInfo->serial_vendorIdentifier
             << __struct_portInfo->serial_productIdentifier;
        __ui_serialsetting_lower->comboBox_serialInfo->addItem(list.first(), list);
    }
}


void serialSetting_vBoxLayout::set_portInfo_lower(int index)
{
    if(index == -1)
        return;
    const QStringList list = __ui_serialsetting_lower->comboBox_serialInfo->itemData(index).toStringList();
    __ui_serialsetting_lower->label_description->setText("Description : " + list.at(1));
    __ui_serialsetting_lower->label_manufacterer->setText("Manufacterer : " + list.at(2));
    __ui_serialsetting_lower->label_serialNumber->setText("Serial Number :" + list.at(3));
    __ui_serialsetting_lower->label_systemLocation->setText("Location :" + list.at(4));
    __ui_serialsetting_lower->label_vendorIdentifier->setText("Vendor identifier :" + list.at(5));
    __ui_serialsetting_lower->label_productIdentifier->setText("Product identifier :" + list.at(6));

    update_portSetting_lower();
}


void serialSetting_vBoxLayout::fill_portSetting_upper()
{
    __ui_serialsetting_upper->comboBox_baudrate->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    __ui_serialsetting_upper->comboBox_baudrate->addItem(QStringLiteral("19200"),QSerialPort::Baud19200);
    __ui_serialsetting_upper->comboBox_baudrate->addItem(QStringLiteral("38400"),QSerialPort::Baud38400);
    __ui_serialsetting_upper->comboBox_baudrate->addItem(QStringLiteral("115200"),QSerialPort::Baud115200);

    __ui_serialsetting_upper->comboBox_dataBits->addItem(QStringLiteral("5"), QSerialPort::Data5);
    __ui_serialsetting_upper->comboBox_dataBits->addItem(QStringLiteral("6"), QSerialPort::Data6);
    __ui_serialsetting_upper->comboBox_dataBits->addItem(QStringLiteral("7"), QSerialPort::Data7);
    __ui_serialsetting_upper->comboBox_dataBits->addItem(QStringLiteral("8"), QSerialPort::Data8);


    __ui_serialsetting_upper->comboBox_parity->addItem(tr("None"), QSerialPort::NoParity);
    __ui_serialsetting_upper->comboBox_parity->addItem(tr("Even"), QSerialPort::EvenParity);
    __ui_serialsetting_upper->comboBox_parity->addItem(tr("Odd"), QSerialPort::OddParity);
    __ui_serialsetting_upper->comboBox_parity->addItem(tr("Mark"), QSerialPort::MarkParity);
    __ui_serialsetting_upper->comboBox_parity->addItem(tr("Space"), QSerialPort::SpaceParity);


    __ui_serialsetting_upper->comboBox_stopBits->addItem(QStringLiteral("1"), QSerialPort::OneStop);
    __ui_serialsetting_upper->comboBox_stopBits->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

}


void serialSetting_vBoxLayout::fill_portSetting_lower()
{
    __ui_serialsetting_lower->comboBox_baudrate->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    __ui_serialsetting_lower->comboBox_baudrate->addItem(QStringLiteral("19200"),QSerialPort::Baud19200);
    __ui_serialsetting_lower->comboBox_baudrate->addItem(QStringLiteral("38400"),QSerialPort::Baud38400);
    __ui_serialsetting_lower->comboBox_baudrate->addItem(QStringLiteral("115200"),QSerialPort::Baud115200);

    __ui_serialsetting_lower->comboBox_dataBits->addItem(QStringLiteral("5"), QSerialPort::Data5);
    __ui_serialsetting_lower->comboBox_dataBits->addItem(QStringLiteral("6"), QSerialPort::Data6);
    __ui_serialsetting_lower->comboBox_dataBits->addItem(QStringLiteral("7"), QSerialPort::Data7);
    __ui_serialsetting_lower->comboBox_dataBits->addItem(QStringLiteral("8"), QSerialPort::Data8);


    __ui_serialsetting_lower->comboBox_parity->addItem(tr("None"), QSerialPort::NoParity);
    __ui_serialsetting_lower->comboBox_parity->addItem(tr("Even"), QSerialPort::EvenParity);
    __ui_serialsetting_lower->comboBox_parity->addItem(tr("Odd"), QSerialPort::OddParity);
    __ui_serialsetting_lower->comboBox_parity->addItem(tr("Mark"), QSerialPort::MarkParity);
    __ui_serialsetting_lower->comboBox_parity->addItem(tr("Space"), QSerialPort::SpaceParity);

    __ui_serialsetting_lower->comboBox_stopBits->addItem(QStringLiteral("1"), QSerialPort::OneStop);
    __ui_serialsetting_lower->comboBox_stopBits->addItem(QStringLiteral("2"), QSerialPort::TwoStop);
}


void serialSetting_vBoxLayout::update_portSetting_upper()
{
    __struct_serialPortSetting_upper->name = __ui_serialsetting_upper->comboBox_serialInfo->currentText();

    __struct_serialPortSetting_upper->baudRate = static_cast<QSerialPort::BaudRate>(
                __ui_serialsetting_upper->comboBox_baudrate->itemData(__ui_serialsetting_upper->comboBox_baudrate->currentIndex()).toInt());
    __struct_serialPortSetting_upper->stringBaudRate = QString::number(__struct_serialPortSetting_upper->baudRate);


    __struct_serialPortSetting_upper->dataBits = static_cast<QSerialPort::DataBits>(
                __ui_serialsetting_upper->comboBox_dataBits->itemData(__ui_serialsetting_upper->comboBox_dataBits->currentIndex()).toInt());
    __struct_serialPortSetting_upper->stringDataBits = __ui_serialsetting_upper->comboBox_dataBits->currentText();


    __struct_serialPortSetting_upper->parity   = static_cast<QSerialPort::Parity>(
                __ui_serialsetting_upper->comboBox_parity->itemData(__ui_serialsetting_upper->comboBox_parity->currentIndex()).toInt());
    __struct_serialPortSetting_upper->stringParity = __ui_serialsetting_upper->comboBox_parity->currentText();


    __struct_serialPortSetting_upper->stopBits = static_cast<QSerialPort::StopBits>(
                __ui_serialsetting_upper->comboBox_stopBits->itemData(__ui_serialsetting_upper->comboBox_stopBits->currentIndex()).toInt());
    __struct_serialPortSetting_upper->stringStopBits = __ui_serialsetting_upper->comboBox_stopBits->currentText();

}


void serialSetting_vBoxLayout::update_portSetting_lower()
{

    __struct_serialPortSetting_lower->name = __ui_serialsetting_lower->comboBox_serialInfo->currentText();

    __struct_serialPortSetting_lower->baudRate = static_cast<QSerialPort::BaudRate>(
                __ui_serialsetting_lower->comboBox_baudrate->itemData(__ui_serialsetting_lower->comboBox_baudrate->currentIndex()).toInt());
    __struct_serialPortSetting_lower->stringBaudRate = QString::number(__struct_serialPortSetting_lower->baudRate);


    __struct_serialPortSetting_lower->dataBits = static_cast<QSerialPort::DataBits>(
                __ui_serialsetting_lower->comboBox_dataBits->itemData(__ui_serialsetting_lower->comboBox_dataBits->currentIndex()).toInt());
    __struct_serialPortSetting_lower->stringDataBits = __ui_serialsetting_lower->comboBox_dataBits->currentText();


    __struct_serialPortSetting_lower->parity   = static_cast<QSerialPort::Parity>(
                __ui_serialsetting_lower->comboBox_parity->itemData(__ui_serialsetting_lower->comboBox_parity->currentIndex()).toInt());
    __struct_serialPortSetting_lower->stringParity = __ui_serialsetting_lower->comboBox_parity->currentText();


    __struct_serialPortSetting_lower->stopBits = static_cast<QSerialPort::StopBits>(
                __ui_serialsetting_lower->comboBox_stopBits->itemData(__ui_serialsetting_lower->comboBox_stopBits->currentIndex()).toInt());
    __struct_serialPortSetting_lower->stringStopBits = __ui_serialsetting_lower->comboBox_stopBits->currentText();

}


serialSetting_vBoxLayout::~serialSetting_vBoxLayout()
{

}


UI_ADS_operation_tableLayout::UI_ADS_operation_tableLayout(QWidget *parent){
    tableWidget_ADS_operation = new QTableWidget;
}


UI_ADS_operation_tableLayout::~UI_ADS_operation_tableLayout(){

}


ADS_operation_tableLayout::ADS_operation_tableLayout(QWidget *parent){
    __ui_ADS_operation_tableLayout = new UI_ADS_operation_tableLayout(parent);

    create_ADS_operation_tableLayout(parent);
}


ADS_operation_tableLayout::~ADS_operation_tableLayout(){

}

void ADS_operation_tableLayout::create_ADS_operation_tableLayout(QWidget *parent){
    QVBoxLayout *b = new QVBoxLayout(parent);

    __ui_ADS_operation_tableLayout->tableWidget_ADS_operation->setRowCount(4);
    __ui_ADS_operation_tableLayout->tableWidget_ADS_operation->setColumnCount(3);
    __ui_ADS_operation_tableLayout->tableWidget_ADS_operation->setColumnWidth(0, 80);
    __ui_ADS_operation_tableLayout->tableWidget_ADS_operation->setColumnWidth(1, 350);
    __ui_ADS_operation_tableLayout->tableWidget_ADS_operation->setColumnWidth(2, 100);

    b->addWidget(__ui_ADS_operation_tableLayout->tableWidget_ADS_operation);
}






