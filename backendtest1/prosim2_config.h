#define prosim2_config
#ifdef prosim2_config

#include "ecgfunction.h"
#include "arrhythmia.h"
#include "perftest.h"
#include <string>




typedef enum {
    ECGFUNC,
    ARRYTHMIA,
    PERFTEST,
    PACEMAKER,
    SETUP
}enum_prosim2Mode_t ;


class Prosim2_config{
    public:
        Prosim2_config();
        enum_prosim2Mode_t get_currentMode();
        void set_currentMode(enum_prosim2Mode_t *__mode);
        struct_ECGFuncConfig_t get_currentECGFunction_config();
        void set_currentECGFunction_config(enum_ECGFunc_mode *__mode,int *value);
        void set_currentECGFunction_config(enum_ECGFunc_mode *__mode,double *value);
        void set_currentECGFunction_config(enum_ECGFunc_mode *__mode,bool *value);
        void set_currentECGFunction_config(enum_ECGFunc_mode *__mode,enum_artifact_t *value);
        vector<string> get_currentCommand(enum_prosim2Mode_t *__mode);

        void set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_supraventricularMode_t *__semimode);
        void set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_prematureMode_t *__semimode);
        void set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_ventricularMode_t *__semimode);
        void set_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_conductionDefectMode_t *__semimode);

        struct_performanceWave_config_t get_currentPerformanceWave_config(enum_perfTest_mode_t *__mode);
        struct_R_waveDetection_config_t get_current_R_waveDetection_config(enum_perfTest_mode_t *__mode);
        void set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_performanceWaves_mode_t *__semimode, int *value);
        void set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_performanceWaves_mode_t *__semimode, double *value);
        void set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_R_wave_mode_t *__semimode, int *value);
        void set_currentPerfTest_config(enum_perfTest_mode_t *__mode, enum_R_wave_mode_t *__semimode, double *value);


    private:
        enum_prosim2Mode_t *__enum_mode = NULL;
        struct_ECGFuncConfig_t *__struct_ECG_config = NULL;
        enum_ECGFunc_mode *__enum_ECGFunc_mode = NULL;
        ECGFunction_config *__ECGFunction_config = NULL;
        arrhythmia_config *__arrhythmia_config = NULL;
        perftest_config *__perftest_config = NULL;
        
};


#endif