#include "ADS1292R.h"

//---- Frequently change config ----
const uint8_t ADS1292R_CHSET_PGA_GAIN_SETTING 				= ADS1292R_CHSET_PGA_GAIN_SIX;
const uint8_t ADS1292R_CONFIG1_SAMPLING_RATE_SETTING  = ADS1292R_CONFIG1_SAMPLING_RATE_500;

//---- Imply config for signal mode ----
#ifdef ADS1292R_TEST_SIGNAL
	const uint8_t ADS1292R_CONFIG2_INT_TEST_SETTING 	= ADS1292R_CONFIG2_INT_TEST_ENA;
	const uint8_t ADS1292R_CHSET_MUX_SETTING					= ADS1292R_CHSET_MUX_TEST_SIGNAL;
#else
	const uint8_t ADS1292R_CONFIG2_INT_TEST_SETTING 	= ADS1292R_CONFIG2_INT_TEST_DIS;
	const uint8_t ADS1292R_CHSET_MUX_SETTING					= ADS1292R_CHSET_MUX_NORMAL;
#endif

//---- begin ----
bool ADS1292R::begin(uint8_t CS, uint8_t numCh){
	vPrint(F("ADS1292R Initializing with Channel Number = "));
	vPrintln(numCh);
	vPrint(F("Signal Mode: "));
	#ifdef ADS1292R_TEST_SIGNAL
		vPrintln(F("Test Signal Mode"));
	#else
		vPrintln(F("Normal Mode"));
	#endif
	if(numCh > ADS1292R_MAX_CHANNEL){
		Serial.println(F("!!!! ERROR !!!! "));
		Serial.print(F("Selected Channel Number exceed maximum channel, only "));
		Serial.print(ADS1292R_MAX_CHANNEL);
		Serial.println(F(" are allowed."));
		Serial.println(F("ADS1292R Initialize FAIL!!!"));
		return false;
	}

	_CS 	 				= CS;
	_NUM_CHANNEL  = numCh;
	_PWDN  	= ADS1292R_PWDN;
	_START 	= ADS1292R_START;

	SPI.begin();
	pinMode(_CS,    OUTPUT);
	pinMode(_PWDN,  OUTPUT);
	pinMode(_START, OUTPUT);
	digitalWrite(_CS, 	 HIGH);
	digitalWrite(_PWDN,  HIGH);
	digitalWrite(_START, LOW);
	delay(100); //wait supply stable after power up

	ADS1292R::commandSend(ADS1292R_COMM_RESET);
	delay(100);	//wait 18tclk
	ADS1292R::commandSend(ADS1292R_COMM_SDATAC);	//stop continuous convert
	delay(10);

	// /*!!---- Default clk_div set for 512K clk, so change clk_div to 2.048M first ----!!*/
	// ADS1292R::WREG(ADS1292R_REGIS_LOFF_STAT,ADS1292R_LOFF_STAT_MASK 
	// 																			| ADS1292R_LOFF_STAT_CLK_DIV_512K);	
	// delay(10);

/*---- Write Config 1 Setting ----*/
	ADS1292R::WREG(ADS1292R_REGIS_CONFIG1,ADS1292R_CONFIG1_MASK 
																			| ADS1292R_CONFIG1_CONT
																			| ADS1292R_CONFIG1_SAMPLING_RATE_SETTING);

/*---- Write Config 2 Setting ----*/
	ADS1292R::WREG(ADS1292R_REGIS_CONFIG2,ADS1292R_CONFIG2_MASK 
																			| ADS1292R_CONFIG2_LOFF_COMP_DIS
																			| ADS1292R_CONFIG2_REFBUF_ENA
																			| ADS1292R_CONFIG2_VREF_2V42
																			| ADS1292R_CONFIG2_CLK_OUT_DIS
																			| ADS1292R_CONFIG2_INT_TEST_SETTING
																			| ADS1292R_CONFIG2_TEST_FREQ_1HZ);
	
/*---- Write Lead Off Setting ----*/
	ADS1292R::WREG(ADS1292R_REGIS_LOFF,		ADS1292R_LOFF_MASK 
																			| ADS1292R_LOFF_COMP_TH_95
																			| ADS1292R_LOFF_ILEAD_6N
																			| ADS1292R_LOFF_FLEAD_DC);

/*---- Enable Channel ----*/
const uint8_t _CHANNEL_CONFIG_ENA = ADS1292R_CHSET_MASK
																	|	ADS1292R_CHSET_PD_NORMAL
																	| ADS1292R_CHSET_PGA_GAIN_SETTING
																	| ADS1292R_CHSET_MUX_SETTING;
for(int i=0; i<_NUM_CHANNEL; i++){
	ADS1292R::WREG(ADS1292R_REGIS_CH1SET+i, _CHANNEL_CONFIG_ENA);
}
/*---- Disable the other channel ----*/
const uint8_t _CHANNEL_CONFIG_DIS = ADS1292R_CHSET_MASK
																	| ADS1292R_CHSET_PD_POWER_DOWN
																	| ADS1292R_CHSET_MUX_SHORTED;
for(int i=_NUM_CHANNEL; i<ADS1292R_MAX_CHANNEL; i++ ){
	ADS1292R::WREG(ADS1292R_REGIS_CH1SET+i, _CHANNEL_CONFIG_DIS);
}
/*---- Write RLD Sense Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_RLD_SENS, ADS1292R_RLD_SENS_MASK 
																			| ADS1292R_RLD_SENS_CHOP_16
																			| ADS1292R_RLD_SENS_RLD_ENA
																			| ADS1292R_RLD_SENS_RLD_LOFF_DIS
																			| ADS1292R_RLD_SENS_RLD2N_DIS
																			| ADS1292R_RLD_SENS_RLD2P_DIS
																			| ADS1292R_RLD_SENS_RLD1N_DIS
																			| ADS1292R_RLD_SENS_RLD1P_DIS);

/*---- Write Lead-Off Sense Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_LOFF_SENS,ADS1292R_LOFF_SENS_MASK 
																			| ADS1292R_LOFF_SENS_FLIP2_DIS
																			| ADS1292R_LOFF_SENS_FLIP1_DIS
																			| ADS1292R_LOFF_SENS_CH2N_DIS
																			| ADS1292R_LOFF_SENS_CH2P_DIS
																			| ADS1292R_LOFF_SENS_CH1N_DIS
																			| ADS1292R_LOFF_SENS_CH1P_DIS);

/*---- Write Lead-Off Status Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_LOFF_STAT,ADS1292R_LOFF_STAT_MASK 
																			| ADS1292R_LOFF_STAT_CLK_DIV_2_048M);

/*---- Write Respiratory Control 1 Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_RESP1,ADS1292R_RESP1_MASK 
																	| ADS1292R_RESP1_DEMOD_CH1_DIS
																	| ADS1292R_RESP1_MOD_CH1_DIS
																	| ADS1292R_RESP1_PHASE_0
																	| ADS1292R_RESP1_RESP_CTRL_INT_CLK);

/*---- Write Respiratory Control 2 Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_RESP2,ADS1292R_RESP2_MASK 
																	| ADS1292R_RESP2_OFFSET_CALIB_DIS
																	| ADS1292R_RESP2_RESP_FREQ_32K
																	| ADS1292R_RESP2_RLDREF_INT);
/*---- Write GPIO Setting ----*/
ADS1292R::WREG(ADS1292R_REGIS_GPIO,	ADS1292R_GPIO_MASK 
																	| ADS1292R_GPIO_2_INPUT
																	| ADS1292R_GPIO_1_INPUT);

	uint8_t _WHO_I_AM = ADS1292R::RREG(ADS1292R_REGIS_ID);
	dPrint(F("ADS1292R Who I am  = "));
	dbPrintln(_WHO_I_AM, BIN);

	ADS1292R::commandSend(ADS1292R_COMM_START);  //Start and sync
	ADS1292R::commandSend(ADS1292R_COMM_RDATAC);  //set to continuous convert mode
	delay(10);
	// digitalWrite(_START, HIGH);
	// delayMicroseconds(20); 											

	if(_WHO_I_AM == I_AM_ADS129R){
		vPrintln(F("ADS1292R Initialize Complete."));
		return true;
	}else{
		Serial.println(F("ADS1292R Initialize FAIL!!!"));
		return false;
	}
}

void ADS1292R::WREG(byte address, byte value){
	uint8_t opcode1 = 0x40|address; 	//  WREG expects 010rrrrr where rrrrr = _address
	ADS1292R_BEGIN_TRANS;
    SPI.transfer(opcode1);					//  Send WREG command & address
    SPI.transfer(0x00);							//	Send number of registers to write -1
    SPI.transfer(value);						//  Write the value to the register
	ADS1292R_END_TRANS;
}

uint8_t ADS1292R::RREG(byte address){
	ADS1292R_BEGIN_TRANS;
		SPI.transfer(0x20|address); //  RREG expects 001rrrrr where rrrrr = _address
		SPI.transfer(0x00); 				//	Send number of registers to read -1
		uint8_t readB = SPI.transfer(0x00);
	ADS1292R_END_TRANS;
	return readB;
}

void ADS1292R::commandSend(byte comm){
	ADS1292R_BEGIN_TRANS;
		SPI.transfer(comm);
	ADS1292R_END_TRANS;
}

bool ADS1292R::read(){
	ADS1292R_BEGIN_TRANS;
		//---- Read data from register ----
			//---- Read 3 Byte of status ----
			// stats = (1100 + LOFF_STAT[4:0] + GPIO[1:0] + 13 '0's)
		convertArray.uint8[3] = 0;
		convertArray.uint8[2] = SPI.transfer(0x00);
		convertArray.uint8[1] = SPI.transfer(0x00);
		convertArray.uint8[0] = SPI.transfer(0x00);
		_status = convertArray.int32;
		
		for(int chNum=0; chNum<ADS1292R_MAX_CHANNEL; chNum++){
		convertArray.uint8[3] = SPI.transfer(0x00);
		convertArray.uint8[2] = SPI.transfer(0x00);
		convertArray.uint8[1] = SPI.transfer(0x00);
		convertArray.uint8[0] = 0;
		convertArray.int32   /= 256;
		_data[chNum] = convertArray.int32;
		}
	ADS1292R_END_TRANS;
	return true;
}

int32_t ADS1292R::getData (uint8_t chNum){
	if(chNum > _NUM_CHANNEL){
		return(0);
	}
	return _data[chNum];
}
int32_t* ADS1292R::getData(){
	return _data;
}