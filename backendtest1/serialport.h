#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>
#include <QList>
#include <QVector>

typedef struct
{
    QString serial_portName;
    QString serial_manufacturer;
    QString serial_serialNumber;
    QString serial_productIdentifier;
    QString serial_description;
    QString serial_systemLocation;
    QString serial_vendorIdentifier;

}struct_serialPortInfo_t;

typedef enum:int
{
    PROSIM2     = 1,
    ARDUINO_DUE = 2,
    ERROR       = 0
}enum_portType;


typedef struct  {
    QString name;
    qint32 baudRate;
    QString stringBaudRate;
    QSerialPort::DataBits dataBits;
    QString stringDataBits;
    QSerialPort::Parity parity;
    QString stringParity;
    QSerialPort::StopBits stopBits;
    QString stringStopBits;
    QSerialPort::FlowControl flowControl;
    QString stringFlowControl;
    bool localEchoEnabled;
}struct_settings_t;

class serialPort
{
public:
    serialPort();
    void set_struct_currentSerialPortInfo(struct_serialPortInfo_t *info);
    struct_serialPortInfo_t get_struct_currentSerialPortInfo();
    void set_struct_value_currentSerialPortSettings(struct_settings_t settings);
    void set_struct_string_currentSerialPortSettings(struct_settings_t settings);
    struct_settings_t get_struct_currentSerialPortSettings();
    void set_current_serialPortpointer(QString portname);
    void set_serialPort_setting(struct_settings_t port);
    void set_portType(enum_portType portType);
    enum_portType get_currentPortType();

    void open_serialPort();
    void write_serialPort(const QByteArray data);
    const QByteArray read_serialPort();
    void disconnect_serialPort();



private:
    ~serialPort();
    QList<QSerialPortInfo> __serialPort_amount;
    QSerialPort *__serialPort;
    QSerialPortInfo *__serialPortInfo;
    struct_serialPortInfo_t *__struct_serialPortInfo;
    QVector <QString> *__vector_AllSerialPort;
    QVector <struct_serialPortInfo_t> *__vectorStruct_allSeiralPortInfo;
    struct_settings_t *__serialPortSettings;
    enum_portType   *__enum_portType;
};



#endif // SERIALPORT_H
