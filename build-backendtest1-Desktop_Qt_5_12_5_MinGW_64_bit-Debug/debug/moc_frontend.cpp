/****************************************************************************
** Meta object code from reading C++ file 'frontend.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../backendtest1/frontend.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frontend.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_UI_serialSetting_vBoxLayout_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UI_serialSetting_vBoxLayout_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UI_serialSetting_vBoxLayout_t qt_meta_stringdata_UI_serialSetting_vBoxLayout = {
    {
QT_MOC_LITERAL(0, 0, 27) // "UI_serialSetting_vBoxLayout"

    },
    "UI_serialSetting_vBoxLayout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UI_serialSetting_vBoxLayout[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void UI_serialSetting_vBoxLayout::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject UI_serialSetting_vBoxLayout::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_UI_serialSetting_vBoxLayout.data,
    qt_meta_data_UI_serialSetting_vBoxLayout,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *UI_serialSetting_vBoxLayout::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UI_serialSetting_vBoxLayout::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_UI_serialSetting_vBoxLayout.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int UI_serialSetting_vBoxLayout::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_serialSetting_vBoxLayout_t {
    QByteArrayData data[11];
    char stringdata0[206];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_serialSetting_vBoxLayout_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_serialSetting_vBoxLayout_t qt_meta_stringdata_serialSetting_vBoxLayout = {
    {
QT_MOC_LITERAL(0, 0, 24), // "serialSetting_vBoxLayout"
QT_MOC_LITERAL(1, 25, 18), // "set_portInfo_upper"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 5), // "index"
QT_MOC_LITERAL(4, 51, 19), // "fill_portInfo_upper"
QT_MOC_LITERAL(5, 71, 18), // "set_portInfo_lower"
QT_MOC_LITERAL(6, 90, 19), // "fill_portInfo_lower"
QT_MOC_LITERAL(7, 110, 22), // "fill_portSetting_upper"
QT_MOC_LITERAL(8, 133, 22), // "fill_portSetting_lower"
QT_MOC_LITERAL(9, 156, 24), // "update_portSetting_upper"
QT_MOC_LITERAL(10, 181, 24) // "update_portSetting_lower"

    },
    "serialSetting_vBoxLayout\0set_portInfo_upper\0"
    "\0index\0fill_portInfo_upper\0"
    "set_portInfo_lower\0fill_portInfo_lower\0"
    "fill_portSetting_upper\0fill_portSetting_lower\0"
    "update_portSetting_upper\0"
    "update_portSetting_lower"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_serialSetting_vBoxLayout[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x0a /* Public */,
       4,    0,   57,    2, 0x0a /* Public */,
       5,    1,   58,    2, 0x0a /* Public */,
       6,    0,   61,    2, 0x0a /* Public */,
       7,    0,   62,    2, 0x0a /* Public */,
       8,    0,   63,    2, 0x0a /* Public */,
       9,    0,   64,    2, 0x0a /* Public */,
      10,    0,   65,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void serialSetting_vBoxLayout::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<serialSetting_vBoxLayout *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_portInfo_upper((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->fill_portInfo_upper(); break;
        case 2: _t->set_portInfo_lower((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->fill_portInfo_lower(); break;
        case 4: _t->fill_portSetting_upper(); break;
        case 5: _t->fill_portSetting_lower(); break;
        case 6: _t->update_portSetting_upper(); break;
        case 7: _t->update_portSetting_lower(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject serialSetting_vBoxLayout::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_serialSetting_vBoxLayout.data,
    qt_meta_data_serialSetting_vBoxLayout,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *serialSetting_vBoxLayout::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *serialSetting_vBoxLayout::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_serialSetting_vBoxLayout.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int serialSetting_vBoxLayout::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
struct qt_meta_stringdata_UI_ADS_operation_tableLayout_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UI_ADS_operation_tableLayout_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UI_ADS_operation_tableLayout_t qt_meta_stringdata_UI_ADS_operation_tableLayout = {
    {
QT_MOC_LITERAL(0, 0, 28) // "UI_ADS_operation_tableLayout"

    },
    "UI_ADS_operation_tableLayout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UI_ADS_operation_tableLayout[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void UI_ADS_operation_tableLayout::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject UI_ADS_operation_tableLayout::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_UI_ADS_operation_tableLayout.data,
    qt_meta_data_UI_ADS_operation_tableLayout,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *UI_ADS_operation_tableLayout::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UI_ADS_operation_tableLayout::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_UI_ADS_operation_tableLayout.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int UI_ADS_operation_tableLayout::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_ADS_operation_tableLayout_t {
    QByteArrayData data[1];
    char stringdata0[26];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ADS_operation_tableLayout_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ADS_operation_tableLayout_t qt_meta_stringdata_ADS_operation_tableLayout = {
    {
QT_MOC_LITERAL(0, 0, 25) // "ADS_operation_tableLayout"

    },
    "ADS_operation_tableLayout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ADS_operation_tableLayout[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void ADS_operation_tableLayout::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ADS_operation_tableLayout::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_ADS_operation_tableLayout.data,
    qt_meta_data_ADS_operation_tableLayout,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ADS_operation_tableLayout::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ADS_operation_tableLayout::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ADS_operation_tableLayout.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ADS_operation_tableLayout::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Mainwindow_t {
    QByteArrayData data[4];
    char stringdata0[65];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Mainwindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Mainwindow_t qt_meta_stringdata_Mainwindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "Mainwindow"
QT_MOC_LITERAL(1, 11, 25), // "connect_serialPort_prosim"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 26) // "connect_serialPort_arduino"

    },
    "Mainwindow\0connect_serialPort_prosim\0"
    "\0connect_serialPort_arduino"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Mainwindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x0a /* Public */,
       3,    0,   25,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Mainwindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Mainwindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connect_serialPort_prosim(); break;
        case 1: _t->connect_serialPort_arduino(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Mainwindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_Mainwindow.data,
    qt_meta_data_Mainwindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Mainwindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Mainwindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Mainwindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Mainwindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
