#ifndef __ADS1292R_H_
#define __ADS1292R_H_

// #define USING_CONFIG_H				//Define this if want to use external config script
// #define USING_MACRO_PRINT_H		//Define this if want to use external macroPrint scrip

#include <Arduino.h>
#include <SPI.h>

//---- Manually configuration if NOT use config.h ----
#ifndef USING_CONFIG_H
	// #define ADS1292R_TEST_SIGNAL
	// #define DEBUG_MODE
	// #define VERBOSE_MODE

	#define ADS1292R_NUM_CHANNEL 	2

	#define ADS1292R_CS_PIN     	2
	#define ADS1292R_DRDY_PIN   	3

	#define ADS1292R_START   			4
	#define ADS1292R_PWDN    			5

	#define ADS1292R_MAX_CHANNEL 	2 
#else
	#include "config.h"
#endif
//---- Macro for SPI comunication ----//
#define ADS1292R_CS_DELAY			volatile uint16_t ads1292r_delay_cnt = 0;\
															while(ads1292r_delay_cnt < 100)\
																ads1292r_delay_cnt++;

#define ADS1292R_BEGIN_TRANS 	SPI.beginTransaction(SPISettings(500000UL, MSBFIRST, SPI_MODE1));\
															digitalWrite(ADS1292R_CS_PIN, LOW);

#define ADS1292R_END_TRANS		ADS1292R_CS_DELAY;\
															digitalWrite(ADS1292R_CS_PIN, HIGH);\
															SPI.endTransaction();



//---- Use this script if NOT use macroPrint.h ----
#ifndef USING_MACRO_PRINT_H
//**** This script from macroPrint.h v1.0, 14-JAN-17. ****
//---- Debug mode ----
	#ifdef DEBUG_MODE
		#define   dPrint(x)       Serial.print(x)
		#define   dPrintln(x)     Serial.println(x)
		#define   dbPrint(x,y)    Serial.print(x,y)
		#define   dbPrintln(x,y)  Serial.println(x,y)
	#else
		#define   dPrint(x)       ;
		#define   dPrintln(x)     ;
		#define   dbPrint(x,y)    ;
		#define   dbPrintln(x,y)  ;
	#endif
//---- Verbose mode ----
	#ifdef VERBOSE_MODE
		#define   vPrint(x)       Serial.print(x)
		#define   vPrintln(x)     Serial.println(x)
		#define   vbPrint(x,y)    Serial.print(x,y)
		#define   vbPrintln(x,y)  Serial.println(x,y)
	#else
		#define   vPrint(x)       ;
		#define   vPrintln(x)     ;
		#define   vbPrint(x,y)    ;
		#define   vbPrintln(x,y)  ;
	#endif
#else
	#include "macroPrint.h"
#endif
//---- SPI Command ----
#define ADS1292R_COMM_WAKEUP 	 	0x02 // Wake-up from standby mode
#define ADS1292R_COMM_STANDBY 	0x04 // Enter Standby mode
#define ADS1292R_COMM_RESET 		0x06 // Reset the device registers to default
#define ADS1292R_COMM_START 		0x08 // Start and restart (synchronize) conversions
#define ADS1292R_COMM_STOP 			0x0A // Stop conversion
#define ADS1292R_COMM_OFFSETCAL 0x1A // CHannel Offset Calibration

#define ADS1292R_COMM_RDATAC 		0x10 // Enable Read Data Continuous mode (default mode at power-up)
#define ADS1292R_COMM_SDATAC 		0x11 // Stop Read Data Continuous mode
#define ADS1292R_COMM_RDATA 		0x12 // Read data by command; supports multiple read back

#define ADS1292R_COMM_RREG_MASK	0x20 // Write at some register (for example ADS1292R_COMM_RREG + ADS1292R_REGIS_CONFIG2) follow by the number of byte to be read-1 0b000nnnnn
#define ADS1292R_COMM_WREG_MASK	0x40 // Read at some register (for example ADS1292R_COMM_WREG + ADS1292R_REGIS_CONFIG2) follow by the number of byte to be write-1 0b000nnnnn

//---- Register Address ----
#define ADS1292R_REGIS_ID 					0x00
#define ADS1292R_REGIS_CONFIG1 			0x01
#define ADS1292R_REGIS_CONFIG2 			0x02

#define ADS1292R_REGIS_LOFF 				0x03
#define ADS1292R_REGIS_CH1SET 			0x04
#define ADS1292R_REGIS_CH2SET 			0x05

#define ADS1292R_REGIS_RLD_SENS			0x06
#define ADS1292R_REGIS_LOFF_SENS 		0x07
#define ADS1292R_REGIS_LOFF_STAT 		0x08

#define ADS1292R_REGIS_RESP1 				0x09
#define ADS1292R_REGIS_RESP2 				0x0A
#define ADS1292R_REGIS_GPIO 				0x0B

//---- ID Resgister ----
#define I_AM_ADS129R		0b01110011

//---- Config 1 ----
#define ADS1292R_CONFIG1_MASK 									0x00	// Mask for config1 Usage: writeReg(CONFIG1_MASK || CONFIG1_SETTING)
#define ADS1292R_CONFIG1_CONT										0x00	// Continuous (Default)
#define ADS1292R_CONFIG1_SINGLE_SHOT						0x80	// Single shot

#define ADS1292R_CONFIG1_SAMPLING_RATE_8K				0x06
#define ADS1292R_CONFIG1_SAMPLING_RATE_4K				0x05
#define ADS1292R_CONFIG1_SAMPLING_RATE_2K				0x04
#define ADS1292R_CONFIG1_SAMPLING_RATE_1K				0x03
#define ADS1292R_CONFIG1_SAMPLING_RATE_500		  0x02	// (Default)
#define ADS1292R_CONFIG1_SAMPLING_RATE_250		 	0x01
#define ADS1292R_CONFIG1_SAMPLING_RATE_125			0x00

//---- Config 2 ----
#define ADS1292R_CONFIG2_MASK 									0x80	// bit 7 must be 1 Usage: writeReg(CONFIG2_MASK || CONFIG2_SETTING)
#define ADS1292R_CONFIG2_LOFF_COMP_DIS 					0x00 	// Lead-off Comparator powerdown (Default)
#define ADS1292R_CONFIG2_LOFF_COMP_ENA 					0x40	// Lead-off Comparator enable
#define ADS1292R_CONFIG2_REFBUF_DIS							0x00 	// Reference buffer powerdown (Default)
#define ADS1292R_CONFIG2_REFBUF_ENA							0x20 	// Reference buffer enable
#define ADS1292R_CONFIG2_VREF_2V42							0x00 	// Disable 4v reference vref = 2.42V (Default)
#define ADS1292R_CONFIG2_VREF_4V								0x10 	// Enable 4v reference vref = 4.033V
#define ADS1292R_CONFIG2_CLK_OUT_DIS						0x00 	// Not Connect internal clock to CLK pin (Default)
#define ADS1292R_CONFIG2_CLK_OUT_ENA						0x08	// Connect internal clock to CLK pin
#define ADS1292R_CONFIG2_INT_TEST_DIS						0x00 	// Disable test signal (Default)
#define ADS1292R_CONFIG2_INT_TEST_ENA						0x02	// Enable test signal at +-(VREFp-VREFn)/2400
#define ADS1292R_CONFIG2_TEST_FREQ_DC						0x00 	// Test signal at DC (Default)
#define ADS1292R_CONFIG2_TEST_FREQ_1HZ					0x01	// Test signal 1Hz squarewave

//---- Lead-Off Control ----
#define ADS1292R_LOFF_MASK					0x10		// bit 4 must be 1 Usage: writeReg(LOFF_MASK || LOFF_SETTING)

#define ADS1292R_LOFF_COMP_TH_95 		0x00		// Lead-Off comparator Positive side threshold (Neg side = 100-Pos) (Default)
#define ADS1292R_LOFF_COMP_TH_92_5 	0x20
#define ADS1292R_LOFF_COMP_TH_90 		0x40
#define ADS1292R_LOFF_COMP_TH_98_5 	0x60
#define ADS1292R_LOFF_COMP_TH_85		0x80
#define ADS1292R_LOFF_COMP_TH_80 		0xA0
#define ADS1292R_LOFF_COMP_TH_75		0xC0
#define ADS1292R_LOFF_COMP_TH_70		0xE0

#define ADS1292R_LOFF_ILEAD_6N			0x00		// Lead-Off Current magnitude (Default)
#define ADS1292R_LOFF_ILEAD_22N			0x04
#define ADS1292R_LOFF_ILEAD_6U			0x08
#define ADS1292R_LOFF_ILEAD_22U			0x0C

#define ADS1292R_LOFF_FLEAD_DC			0x00 		// Lead-Off Frequency DC (Default)
#define ADS1292R_LOFF_FLEAD_AC			0x01		// Lead-Off Freqency = Fs/4

//---- Channel Set ----
#define	ADS1292R_CHSET_MASK 						0x00
#define ADS1292R_CHSET_PD_NORMAL				0x00 	// Enable (Default)
#define ADS1292R_CHSET_PD_POWER_DOWN		0x80	// Power down

#define ADS1292R_CHSET_PGA_GAIN_SIX			0x00 	// PGA Gain (default = 6times)
#define ADS1292R_CHSET_PGA_GAIN_ONE			0x10 	
#define ADS1292R_CHSET_PGA_GAIN_TWO			0x20
#define ADS1292R_CHSET_PGA_GAIN_THREE		0x30
#define ADS1292R_CHSET_PGA_GAIN_FOUR		0x40
#define ADS1292R_CHSET_PGA_GAIN_EIGHT		0x50
#define ADS1292R_CHSET_PGA_GAIN_TWELVE	0x60

#define ADS1292R_CHSET_MUX_NORMAL				0x00 	// normal (default)
#define ADS1292R_CHSET_MUX_SHORTED			0x01	// Input Shorted (for offset measurement)
#define ADS1292R_CHSET_MUX_RLD_MEA			0x02 	// RLD measure
#define ADS1292R_CHSET_MUX_MVDD					0x03	// Supply measure !!- gain must set to 1 -!! //read Data sheet for MVDDP-MVDDN value
#define ADS1292R_CHSET_MUX_TEMPERATURE	0x04	// Temperature measure
#define ADS1292R_CHSET_MUX_TEST_SIGNAL	0x05	// Test signal
#define ADS1292R_CHSET_MUX_RLD_DRP			0x06 	// Positive input connect to RLDIN
#define ADS1292R_CHSET_MUX_RLD_DRM			0x07 	// Negative input connect to RLDIN
#define ADS1292R_CHSET_MUX_RLD_DRPM			0x08 	// Both input connect to RLDIN
#define ADS1292R_CHSET_MUX_ROUTE_3_1 		0x09  // Route IN3P and IN3N to Channel 1 inputs

//---- Right Leg Drive Sense Selection ----
#define ADS1292R_RLD_SENS_MASK 					0x00 	
#define ADS1292R_RLD_SENS_CHOP_16				0x00 	// Chop frequency = F_mod/16
#define ADS1292R_RLD_SENS_CHOP_2				0x80	// F_Chop = F_mod/2
#define ADS1292R_RLD_SENS_CHOP_4				0xC0	// F_Chop = F_mod/4

#define ADS1292R_RLD_SENS_RLD_DIS 			0x00 	// powerdown RLD buffer (default)
#define ADS1292R_RLD_SENS_RLD_ENA				0x20  // RLD buffer enable
#define ADS1292R_RLD_SENS_RLD_LOFF_DIS	0x00	// RLD Lead-Off sense disable (default)
#define ADS1292R_RLD_SENS_RLD_LOFF_ENA	0x10	// RLD Lead-Off sense enable 

#define ADS1292R_RLD_SENS_RLD2N_DIS			0x00	// not include Neg-Input ch2 for RLD derivation (default)
#define ADS1292R_RLD_SENS_RLD2N_ENA			0x08	// connect Neg-Input ch2 for RLD derivation
#define ADS1292R_RLD_SENS_RLD2P_DIS			0x00	// not include Pos-Input ch2 for RLD derivation (default)
#define ADS1292R_RLD_SENS_RLD2P_ENA			0x04	// connect Pos-Input ch2 for RLD derivation

#define ADS1292R_RLD_SENS_RLD1N_DIS			0x00	// not include Neg-Input ch1 for RLD derivation (default)
#define ADS1292R_RLD_SENS_RLD1N_ENA			0x02	// connect Neg-Input ch1 for RLD derivation
#define ADS1292R_RLD_SENS_RLD1P_DIS			0x00	// not include Pos-Input ch1 for RLD derivation (default)
#define ADS1292R_RLD_SENS_RLD1P_ENA			0x01	// connect Pos-Input ch1 for RLD derivation

//---- Lead-Off Sense Selection ----//
#define ADS1292R_LOFF_SENS_MASK 				0x00
#define ADS1292R_LOFF_SENS_FLIP2_DIS		0x00	// Ch2 lead-off current not flip (default)
#define ADS1292R_LOFF_SENS_FLIP2_ENA		0x20	// Ch2 lead-off current flip
#define ADS1292R_LOFF_SENS_FLIP1_DIS		0x00	// Ch1 lead-off current not flip (default)
#define ADS1292R_LOFF_SENS_FLIP1_ENA		0x10	// Ch1 lead-off current flip

#define ADS1292R_LOFF_SENS_CH2N_DIS			0x00	// Ch2N lead-off detection disable (default)
#define ADS1292R_LOFF_SENS_CH2N_ENA			0x08	// Ch2N lead-off detection enable
#define ADS1292R_LOFF_SENS_CH2P_DIS			0x00	// Ch2P lead-off detection disable (default)
#define ADS1292R_LOFF_SENS_CH2P_ENA			0x04	// Ch2P lead-off detection enable
#define ADS1292R_LOFF_SENS_CH1N_DIS			0x00	// Ch1N lead-off detection disable (default)
#define ADS1292R_LOFF_SENS_CH1N_ENA			0x02	// Ch1N lead-off detection enable
#define ADS1292R_LOFF_SENS_CH1P_DIS			0x00	// Ch1P lead-off detection disable (default)
#define ADS1292R_LOFF_SENS_CH1P_ENA			0x01	// Ch1P lead-off detection enable

//---- Lead-Off Status ----//
#define ADS1292R_LOFF_STAT_MASK							0x00
#define ADS1292R_LOFF_STAT_CLK_DIV_512K 		0x00 // Fmod = Fclk/4 for use with Fclk 512K (Default)
#define ADS1292R_LOFF_STAT_CLK_DIV_2_048M 	0x40 // Fmod = Fclk/16 for use with Fclk 2.048M

#define ADS1292R_LOFF_STAT_MASK_IS_RLD_OFF	0x10 // RLD LOFF status mask Ex: isXOff = read(LOFF_STAT_REG) && LOFF_STAT_MASK_IS_X_OFF
#define ADS1292R_LOFF_STAT_MASK_IS_IN2N_OFF	0x08 // IN2N LOFF Mask
#define ADS1292R_LOFF_STAT_MASK_IS_IN2P_OFF	0x04 // IN2P LOFF Mask
#define ADS1292R_LOFF_STAT_MASK_IS_IN1N_OFF	0x02 // IN1N LOFF Mask
#define ADS1292R_LOFF_STAT_MASK_IS_IN1P_OFF	0x01 // IN1P LOFF Mask

/*---- Respiration control 1 ----*/
#define ADS1292R_RESP1_MASK 					0x02 // bit 2 must be 1 Usage: writeReg(RESP1_MASK || RESP1_SETTING)

#define ADS1292R_RESP1_DEMOD_CH1_DIS	0x00 // disable respiration demodulation circuitry on ch1 (default)
#define ADS1292R_RESP1_DEMOD_CH1_ENA	0x80 // enable respiration demodulation circuitry on ch1
#define ADS1292R_RESP1_MOD_CH1_DIS		0x00 // disable respiration modulation circuitry on ch1 (default)
#define ADS1292R_RESP1_MOD_CH1_ENA		0x40 // enable respiration modulation circuitry on ch1

#define ADS1292R_RESP1_PHASE_0				0x00 // Phase of respiration demodulation control signal (default) //value for RESP_FREQ = 32kHz; if use 64kHz value x2 and do not use > 78.75 (== 157.5 for clk32kHz)
#define ADS1292R_RESP1_PHASE_11_25		0x04
#define ADS1292R_RESP1_PHASE_22_50		0x08
#define ADS1292R_RESP1_PHASE_33_75		0x0C
#define ADS1292R_RESP1_PHASE_45				0x10
#define ADS1292R_RESP1_PHASE_56_25		0x14
#define ADS1292R_RESP1_PHASE_67_50		0x18
#define ADS1292R_RESP1_PHASE_78_75		0x1C // Phase = 157.50 for RESP_FREQ = 64kHz *!! Do not use value above this !!* //
#define ADS1292R_RESP1_PHASE_90				0x20 // Do not use for RESP_FREQ = 64kHz // Phase = 180 for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_101_25		0x24 // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_112_50		0x28 // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_123_75		0x2C // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_135			0x30 // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_146_25		0x34 // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_157_50		0x38 // Do not use for RESP_FREQ = 64kHz
#define ADS1292R_RESP1_PHASE_168_75		0x3C // Do not use for RESP_FREQ = 64kHz

#define ADS1292R_RESP1_RESP_CTRL_INT_CLK	0x00	// Internal respiration with internal clock (default)
#define ADS1292R_RESP1_RESP_CTRL_EXT_CLK	0x01	// Internal respiration with external clock

/*---- Respiration control 2 ----*/
#define ADS1292R_RESP2_MASK 						0x01 // bit 1 must be 1 Usage: writeReg(RESP2_MASK || RESP2_SETTING)

#define ADS1292R_RESP2_OFFSET_CALIB_DIS	0x00 // Disable offset Calibration (Default)
#define ADS1292R_RESP2_OFFSET_CALIB_ENA	0x80 // Enable offset Calibration 
#define ADS1292R_RESP2_RESP_FREQ_32K		0x00 // Respiratory control internal clock 32kHz (default) //when set RESP_CTRL = 0 
#define ADS1292R_RESP2_RESP_FREQ_64K		0x04 // Respiratory control internal clock 64kHz //when set RESP_CTRL = 0 
#define ADS1292R_RESP2_RLDREF_INT				0x02 // RLD reference signal internally at (AVDD-AVSS)/2 (Default)
#define ADS1292R_RESP2_RLDREF_EXT				0x00 // RLD reference signal fed externally

/*---- GPIO ----*/
#define ADS1292R_GPIO_MASK 				0x00
#define ADS1292R_GPIO_2_INPUT			0x08 	// set GPIO 2 as Input (Default) 
#define ADS1292R_GPIO_2_OUTPUT		0x00	// set GPIO 2 as Output
#define ADS1292R_GPIO_1_INPUT			0x04 	// set GPIO 1 as Input (Default) 
#define ADS1292R_GPIO_1_OUTPUT		0x00	// set GPIO 1 as Output
#define ADS1292R_GPIO_2_DATA			0x02	// write to high if set as output, mask for reading status if set as input 
#define ADS1292R_GPIO_1_DATA			0x01	// write to high if set as output, mask for reading status if set as input 

union ADS1292R_ConvertArray_t{
  int32_t int32;
  uint8_t uint8[4];
};

class ADS1292R
{
	public:
		bool begin(uint8_t CS, uint8_t numCh);
		bool read(void);
		void WREG(byte address, byte value);
		void commandSend(byte comm);
		uint8_t RREG(byte address);
		int32_t getData (uint8_t chNum);
		int32_t* getData(void);

	private:
		int32_t  _data[ADS1292R_MAX_CHANNEL];
		uint32_t _status;
		ADS1292R_ConvertArray_t convertArray;

	protected:
		uint8_t _CS, _PWDN, _START, _NUM_CHANNEL;
};
#endif
